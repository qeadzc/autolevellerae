/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import javax.annotation.Nonnull;
import java.io.PrintWriter;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Resposible for writing gcode to the AL file based on the OGF
 * Created by James Hawthorne on 23/06/2015.
 */
public class ALWriter
{
    private static final Optional<Logger> LOGGER = Autoleveller.getLogger(ALWriter.class.getName());
    private final PrintWriter alWriter;

    private ALWriter(PrintWriter fileWriter)
    {
        alWriter = fileWriter;
    }

    private void close()
    {
        alWriter.close();
    }

    /**
     * Writes a string to the file. Can only be used via the static useALWriter() method
     * @param message The string to write
     */
    public void writeGCODELine(final String message)
    {
        alWriter.println(message);
    }

    /**
     * Method to use an ALWriter instance via the Consumer object.<br>
     * An ALWriter instance is created before executing the Consumer function and closed afterwards in a execute around pattern
     * @param writer The PrintWriter object to be used by ALWriter (can be mocked for testing)
     * @param writerConsumer The Consumer object to execute
     */
    public static void useALWriter(@Nonnull PrintWriter writer, Consumer<ALWriter> writerConsumer)
    {
        ALWriter alWriter = new ALWriter(writer);

        try {
            writerConsumer.accept(alWriter);
        } catch (Exception e) {
            LOGGER.ifPresent(logger -> logger.log(Level.SEVERE, "Writing to file " + writer.toString() +
                    " failed for writing object " + writerConsumer.toString()));
        }
        finally {
            alWriter.close();
        }
    }
}

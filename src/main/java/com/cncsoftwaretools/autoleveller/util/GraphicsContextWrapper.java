/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.util;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * Wraps all used function of GraphicsContext
 * Created because Mockito cannot create final classes, and GraphicsContext is final
 * Created by James Hawthorne on 15/06/2017.
 */
public class GraphicsContextWrapper
{
    private final GraphicsContext gc;
    
    public GraphicsContextWrapper(GraphicsContext gc)
    {
        this.gc = gc;
    }

    public void setFill(Color color)
    {
        gc.setFill(color);
    }

    public void fillRect(double x, double y, double w, double h)
    {
        gc.fillRect(x, y, w, h);
    }

    public void setStroke(Color color)
    {
        gc.setStroke(color);
    }

    public void strokeRect(double x, double y, double w, double h)
    {
        gc.strokeRect(x, y, w, h);
    }

    public void fillOval(double x, double y, double w, double h)
    {
        gc.fillOval(x, y, w, h);
    }

    public void strokePolyline(double[] xs, double[] ys, int pointCount)
    {
        gc.strokePolyline(xs, ys, pointCount);
    }

    public void strokeArc(double x, double y, double w, double h, double startAngle, double arcExtent, ArcType closure)
    {gc.strokeArc(x, y, w, h, startAngle, arcExtent, closure);}

    public void clearRect(int x, int y, double width, double height)
    {
        gc.clearRect(x, y, width, height);
    }

    public void setLineDashOffset(double offset)
    {
        gc.setLineDashOffset(offset);
    }

    public void setDashLengths(double[] lengths)
    {
        gc.setLineDashes(lengths);
    }

    public void strokeline(double x1, double y1, double x2, double y2)
    {
        gc.strokeLine(x1, y1, x2, y2);
    }
}

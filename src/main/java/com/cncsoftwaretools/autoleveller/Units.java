/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.reader.Word;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Units can be millimeters or inches, nothing else
 * Created by James Hawthorne on 11/06/2015.
 */
public enum Units
{
    MM("Millimeters"),
    INCHES("Inches");

    private final static double mmPerInch = 25.4;
    private final String unitsTxt;

    Units(String unitsTxt)
    {
        this.unitsTxt = unitsTxt;
    }

    /**
     * Gets the gcode Word representation of this Unit, i.e. G20 for inches and G21 for millimeters
     */
    Word getWord()
    {
        return unitsTxt.equalsIgnoreCase("Millimeters") ?
                Word.createPair('G', new BigDecimal(21)) : Word.createPair('G', new BigDecimal(20));
    }

    @Override
    public String toString()
    {
        return unitsTxt;
    }

    @SuppressWarnings("unused")
    public static Optional<Units> fromString(String unitsStr)
    {
        if (unitsStr.equals("Inches")){
            return Optional.of(Units.INCHES);
        }

        if (unitsStr.equals("Millimeters")){
            return Optional.of(Units.MM);
        }

        return Optional.empty();
    }

    public static double mmToInches(double mmValue)
    {
        return mmValue / mmPerInch;
    }

    public static double InchesToMM(double inchValue)
    {
        return inchValue * mmPerInch;
    }

}

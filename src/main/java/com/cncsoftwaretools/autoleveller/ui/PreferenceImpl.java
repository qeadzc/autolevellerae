/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.ui;

import com.cncsoftwaretools.autoleveller.Autoleveller;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by James Hawthorne on 15/03/2017.
 *
 */
public class PreferenceImpl implements Preferences
{
    private final Logger LOGGER = Autoleveller.getLogger(
            PreferenceImpl.class.getName()).orElse(Logger.getAnonymousLogger());
    private final Path filename;
    private final Properties props;

    private PreferenceImpl(Path path)
    {
        filename = path;
        createAndInitPrefFileIfNotExists(path);
        props = readPropertiesFile(path);
    }

    private void createAndInitPrefFileIfNotExists(Path path)
    {
        if (Files.notExists(path)) {
            try (PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(path.toFile())))) {
                pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
                pw.println("<!DOCTYPE properties SYSTEM \"http://java.sun.com/dtd/properties.dtd\">");
                pw.println("<properties>");
                pw.println("</properties>");
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "Error creating or writing to properties file " + path
                        + " because: " + e.getMessage());
            }
        }
    }

    private Properties readPropertiesFile(Path path)
    {
        Properties props = new Properties();

        try (InputStream is = new BufferedInputStream(Files.newInputStream(path))){
            props.loadFromXML(is);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Error reading from properties file " + path
                    + " because: " + e.getMessage());
        }
        return props;
    }

    public static Preferences newInstance(Path path)
    {
        return new PreferenceImpl(path);
    }

    @Override
    public Optional<String> get(String key)
    {
        return Optional.ofNullable(props.getProperty(key));
    }

    @Override
    public void set(String key, String value)
    {
        props.setProperty(key, value);
        try(OutputStream os = new BufferedOutputStream(Files.newOutputStream(filename))){
            props.storeToXML(os, "Last Written: " + LocalDateTime.now()
                    .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        } catch (IOException ioe){
            LOGGER.log(Level.WARNING, "Error writing to properties file " + filename
                    + " because: " + ioe.getMessage());
        }
    }

    Path getPath()
    {
        return filename;
    }
}

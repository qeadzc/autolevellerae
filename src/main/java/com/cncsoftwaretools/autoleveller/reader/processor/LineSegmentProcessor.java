/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.ALTextBlockFunctions;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.Word;
import com.cncsoftwaretools.autoleveller.reader.WordGroup;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Segments any line which is > the segment Length
 * Segmented lines have the same length, direction and position as the original
 * they are just segmented into several points so the lines can be levelled more regularly.
 * Created by James Hawthorne on 26/09/2015.
 */
public class LineSegmentProcessor extends GCodeInterpreter
{
    private Optional<GCodeState> lastProcessed = Optional.empty();

    private final BigDecimal segmentLength;
    private final String formattedSegmentLength;
    private final Optional<GCodeProcessor> processor;

    private LineSegmentProcessor(Optional<GCodeProcessor> processor, double segmentLength)
    {
        this.segmentLength = new BigDecimal(segmentLength);
        this.formattedSegmentLength = this.segmentLength.setScale(ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_UP)
                .stripTrailingZeros()
                .toPlainString();
        this.processor = processor;
    }

    /**
     * Creates a new instance of this processor which are chained with additional processors
     * @param processor the GCodeProcessor to pass each segmented (or not) GCodeState to if present
     * @param segmentLength the desired maximum segment length of each line
     * @return an instance of this Segmentor Processor
     */
    public static LineSegmentProcessor newInstance(Optional<GCodeProcessor> processor, double segmentLength)
    {
        return new LineSegmentProcessor(processor, segmentLength);
    }

    @Override
    public List<GCodeState> processState(GCodeState currentState)
    {
        return segment(currentState);
    }

    @Override
    public Optional<GCodeProcessor> nextProcessor()
    {
        return processor;
    }

    @VisibleForTesting
    List<GCodeState> segment(GCodeState endState)
    {
        if (canSegment(endState))
        {
            Point2D prevPoint = endState.getPreviousState().get().get2DPoint().get();
            Point2D currentPoint = endState.get2DPoint().get();
            BigDecimal[] segments = new BigDecimal(prevPoint.distance(currentPoint)).divideAndRemainder(segmentLength);
            int segmentCount = segments[0].intValue();

            //if segments divide exactly then there is no need for the final segment in the following list
            if  (segments[1].compareTo(BigDecimal.ZERO) == 0) segmentCount--;

            Optional<GCodeState> initialState = endState.getPreviousState();

            List<GCodeState> segmentedStates = Stream
                    .iterate(initialState, lastState -> createNewStateSegment(lastState, endState))
                    .map(Optional::get)
                    .skip(1)
                    .limit(segmentCount)
                    .collect(toList());

            lastProcessed = Optional.of(segmentedStates.get(segmentedStates.size() - 1));
            segmentedStates.add(GCodeState.newInstance(lastProcessed, endState.getRawLine()));
            lastProcessed = Optional.of(segmentedStates.get(segmentedStates.size() - 1));

            return ImmutableList.copyOf(segmentedStates);
        }

        lastProcessed = Optional.of(GCodeState.newInstance(lastProcessed, endState.getRawLine()));
        return ImmutableList.of(lastProcessed.get());
    }

    @VisibleForTesting
    boolean canSegment(GCodeState state)
    {
        //check for only G0 and G1 moves
        Optional<Word> movement = state.getModalWordByGroup(WordGroup.MOTIONGROUP);
        if (!movement.isPresent()) return false;

        Character wordChar = movement.get().getWordPair().getFirst();
        BigDecimal wordNumber = movement.get().getWordPair().getSecond();
        if (!(wordChar.equals('G') && (wordNumber.equals(BigDecimal.ZERO)|| wordNumber.equals(BigDecimal.ONE)))) return false;

        if (!state.getPreviousState().isPresent()) return false;

        // if previous state state exists, create a reference to it
        GCodeState previousState = state.getPreviousState().get();

        Optional<Point2D> prevPoint = previousState.get2DPoint();
        if (!prevPoint.isPresent()) return false;

        Optional<Point2D> currPoint = state.get2DPoint();
        if (!currPoint.isPresent()) return false;

        final double twoDDistance = prevPoint.get().distance(currPoint.get());
        //if line shorter than the segment length
        if (twoDDistance <= segmentLength.doubleValue()) return false;

        //check Z is negative
        Optional<Word> zDepth = state.getModalWordByGroup(WordGroup.ZAXISGROUP);
        //noinspection RedundantIfStatement
        if (zDepth.isPresent() &&
                zDepth.get().getWordPair().getSecond().compareTo(LevelerProcessor.SURFACEHEIGHT) == 1) return false;

        Optional<Point3D> curr3DPoint = state.getReal3DPoint();
        Optional<Point3D> prev3DPoint = previousState.getReal3DPoint();

        if (curr3DPoint.isPresent() && prev3DPoint.isPresent()){
            //if z changed and 2D distance changed
            if ((twoDDistance != 0) && (curr3DPoint.get().getZ() - prev3DPoint.get().getZ() != 0)) return false;
        }

        return true;
    }

    private Optional<GCodeState> createNewStateSegment(Optional<GCodeState> previousState, GCodeState endState)
    {
        Point2D stateToPoint = previousState.get().get2DPoint().get();
        Point2D segmentPoint = reduceSegmentLength(stateToPoint, endState.get2DPoint().get());
        String lineToTransform = endState.getRawLine().toUpperCase();

        String newRawLine = lineToTransform
                .replaceAll("X\\s{0,3}" + Word.DECIMALPATTERN.pattern(),
                        "X" + new BigDecimal(segmentPoint.getX()).setScale(ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_UP))
                .replaceAll("Y\\s{0,3}" + Word.DECIMALPATTERN.pattern(),
                        "Y" + new BigDecimal(segmentPoint.getY()).setScale(ALTextBlockFunctions.SCALE, BigDecimal.ROUND_HALF_UP));

        newRawLine = newRawLine.concat(" ;segmented line. Max segment length set to " + formattedSegmentLength);

        GCodeState state = GCodeState.newInstance(previousState, newRawLine);

        return Optional.of(state);
    }

    @VisibleForTesting
    Point2D reduceSegmentLength(Point2D initialPoint, Point2D endPoint)
    {
        double distance = initialPoint.distance(endPoint);

        if (distance > segmentLength.doubleValue()) //if distance > segment length
        {
            double ratio = 1 / (distance / segmentLength.doubleValue());

            // point1.x + ratio * (point2.x - point1.x);
            double newX = initialPoint.getX() + ratio * (endPoint.getX() - initialPoint.getX());
            double newY = initialPoint.getY() + ratio * (endPoint.getY() - initialPoint.getY());

            return new Point2D(newX, newY);
        }
        else
            return endPoint;
    }
}

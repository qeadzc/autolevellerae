/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.reader.GCodeState;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Processes a list of GCodeStates.
 * Abstract class to implement any default methods from GCodeProcessor which should be final.
 * New processors should implement this abstract class rather than GCodeProcessor
 * Created by James Hawthorne on 17/03/2016.
 */
public abstract class GCodeInterpreter implements GCodeProcessor
{
    /**
     * Processes a list of GCodeStates then recursevly processes the resulting list with each nested GCodeProcessor
     * @param states The states to be processed
     */
    @Override
    public final void interpret(List<GCodeState> states)
    {
        List<GCodeState> returnedStates = states
                .stream()
                .flatMap(gCodeState -> processState(gCodeState).stream())
                .collect(toList());

        nextProcessor().ifPresent(gCodeProcessor -> gCodeProcessor.interpret(returnedStates));
    }
}

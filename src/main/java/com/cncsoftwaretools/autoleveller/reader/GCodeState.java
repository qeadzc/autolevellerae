/*  	AutoLevellerAE (http://www.autoleveller.co.uk) is a stand-alone PC application written in Java which is designed
 *  	to measure precisely the height of the material to be milled / etched in several places,
 *  	then use the information gathered to make adjustments to the Z height
 *  	during the milling / etching process so that a more consistent and accurate result can be achieved.
 *
 *   	Copyright (C) 2013 James Hawthorne PhD, daedelus1982@gmail.com
 *
 *   	This program is free software; you can redistribute it and/or modify
 *   	it under the terms of the GNU General Public License as published by
 *   	the Free Software Foundation; either version 2 of the License, or
 *   	(at your option) any later version.
 *
 *   	This program is distributed in the hope that it will be useful,
 *   	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   	GNU General Public License for more details.
 *
 *   	You should have received a copy of the GNU General Public License along
 *   	with this program; if not, see http://www.gnu.org/licenses/
*/
package com.cncsoftwaretools.autoleveller.reader;

import com.cncsoftwaretools.autoleveller.Pair;
import com.cncsoftwaretools.autoleveller.ThreeDPoint;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.math.NumberUtils.isNumber;

/**
 * Shows the current state of the gcode being read up to and including the line read.
 * Modal words (i.e. Words which remain active until overwritten by another of the same group) ex. G01
 * Are held in one list whilst all current Words in the new input are held in another list
 * Created by James Hawthorne on 20/08/2015.
 */
public class GCodeState
{
    private final ImmutableMap<WordGroup, Word> modalWords;
    private final ImmutableList<Word> currentWords;
    private final Optional<GCodeState> previousState;
    private final ImmutableMap<String, BigDecimal> parameters;
    private final int iteration;
    private final String rawLine;

    private static Pattern floatPt = Pattern.compile("-?([0-9]+([\\.,][0-9]*)?|[\\.,][0-9]+)");
    private static final Pattern parameterPattern = Pattern.compile("#[0-9]+");
    private static final Pattern parameterAssignmentPattern = Pattern.compile("(" + parameterPattern + ")\\s*=\\s*(" + floatPt + ")");

    private GCodeState(GCodeState gCodeState)
    {
        this.iteration = gCodeState.iteration;
        this.currentWords = ImmutableList.copyOf(gCodeState.currentWords);
        this.modalWords = ImmutableMap.copyOf(gCodeState.modalWords);
        this.parameters = ImmutableMap.copyOf(gCodeState.parameters);
        this.previousState = Optional.empty(); //maintain a reference to the previous state but not the one before otherwise too many objects would exist
        this.rawLine = gCodeState.getRawLine();
    }

    private GCodeState(Optional<GCodeState> previousState, String newLine)
    {
        String tempLine = previousState.map(gCodeState -> replaceParmsWithValues(newLine, gCodeState.parameters)).orElse(newLine);
        List<Word> wordsFromString = wordsFromLine(tempLine);
        this.currentWords = ImmutableList.copyOf(wordsFromString);
        this.rawLine = newLine;
        Optional<Pair<String, BigDecimal>> parameter = parameterFromLine(newLine);

        if (previousState.isPresent()){
            GCodeState extractedState = previousState.get();
            this.previousState = Optional.of(new GCodeState(extractedState));
            this.iteration = extractedState.iteration + 1;
            this.modalWords = ImmutableMap.copyOf(combineToModalWordMap(extractedState.modalWords.values().asList(), this.currentWords));

            Map<String, BigDecimal> pars = new HashMap<>();
            pars.putAll(extractedState.parameters);
            parameter.ifPresent(par -> pars.put(par.getFirst(), par.getSecond()));
            this.parameters = ImmutableMap.copyOf(pars);
        }
        else
        {
            this.previousState = Optional.empty();
            this.iteration = 1;
            this.modalWords = ImmutableMap.copyOf(combineToModalWordMap(Collections.emptyList(), currentWords));
            if (parameter.isPresent()){
                this.parameters = ImmutableMap.of(parameter.get().getFirst(), parameter.get().getSecond());
            }
            else{
                this.parameters = ImmutableMap.of();
            }
        }

    }

    private Map<WordGroup, Word> combineToModalWordMap(List<Word> words1, List<Word> words2)
    {
        Map<WordGroup, Word> combinedWords = new HashMap<>();

        words1.stream()
                .filter(word -> word.getGroup() != WordGroup.NOGROUP)
                .forEach(word -> combinedWords.put(word.getGroup(), word));
        words2.stream()
                .filter(word -> word.getGroup() != WordGroup.NOGROUP)
                .forEach(word -> combinedWords.put(word.getGroup(), word));

        return ImmutableMap.copyOf(combinedWords);
    }

    /**
     * Creates a new GCodeState using the previous state as a base
     * @param previousState The GCodeState immediately before this one
     * @param line The line from which to create the State
     * @return A GCodeState representing the gcode up to this point
     */
    public static GCodeState newInstance(Optional<GCodeState> previousState, String line)
    {
        return new GCodeState(previousState, line);
    }

    public static List<Word> wordsFromLine(String line)
    {
        List<Word> words = new ArrayList<>();
        String lineWithoutComments = removeComments(line);
        Matcher matcher = Word.WORDPATTERN.matcher(lineWithoutComments.toUpperCase());

        while (matcher.find()){
            char letter = matcher.group(1).charAt(0);
            String value = matcher.group(2).replace(',', '.');

            words.add(Word.createPair(letter, new BigDecimal(value)));
        }

        return ImmutableList.copyOf(words);
    }

    private String replaceParmsWithValues(String line, ImmutableMap<String, BigDecimal> knownParms)
    {
        String newLn = line;
        Matcher parmMatcher = parameterPattern.matcher(newLn);

        while (parmMatcher.find()){
            if (knownParms.containsKey(parmMatcher.group())){
                newLn = parmMatcher.replaceAll(knownParms.get(parmMatcher.group()).toPlainString());
            }
        }

        return newLn;
    }

    private Optional<Pair<String, BigDecimal>> parameterFromLine(String line)
    {
        final String trimedLine = removeComments(line).trim();

        if (!trimedLine.matches(parameterAssignmentPattern.pattern())){
            return Optional.empty();
        }

        final Matcher parameterMatcher = parameterAssignmentPattern.matcher(trimedLine);
        //noinspection ResultOfMethodCallIgnored
        parameterMatcher.find();
        final Pair<String, BigDecimal> parameter = Pair.createPair(parameterMatcher.group(1), new BigDecimal(parameterMatcher.group(2)));
        return Optional.of(parameter);
    }

    public static String removeComments(String line)
    {
        int indexOfOpeningBracket = line.indexOf('(');
        int indexOfClosingBracket = line.lastIndexOf(')');

        //if indexofclosingbrack is at the end of line then indexofclosingbracket+1 would cause an indexoutofboundsexception
        String afterClosingBracket = indexOfClosingBracket == line.length() -1 ?
                "" : line.substring(indexOfClosingBracket+1, line.length()-1);

        String noBracketComments = (indexOfOpeningBracket != -1) && (indexOfClosingBracket != -1) ?
                line.substring(0, indexOfOpeningBracket) + afterClosingBracket :
                line;

        int indexOfSemiColon = noBracketComments.indexOf(';');

        return indexOfSemiColon != -1 ? noBracketComments.substring(0, indexOfSemiColon) : noBracketComments; //remove semi colon type comments
    }

    public Optional<Point2D> get2DPoint()
    {
        Optional<Word> xAxis = getModalWordByGroup(WordGroup.XAXISGROUP);
        Optional<Word> yAxis = getModalWordByGroup(WordGroup.YAXISGROUP);

        if (xAxis.isPresent() && yAxis.isPresent()){
            return Optional.of(new Point2D(
                    xAxis.get().getWordPair().getSecond().doubleValue(),
                    yAxis.get().getWordPair().getSecond().doubleValue()));
        }

        return Optional.empty();
    }

    public Optional<ThreeDPoint> get3DPoint()
    {
        Optional<Point2D> twoDPoint = get2DPoint();
        Optional<Word> zAxis = getModalWordByGroup(WordGroup.ZAXISGROUP);

        if (twoDPoint.isPresent() && zAxis.isPresent()){
            return Optional.of(ThreeDPoint.createPoint(new BigDecimal(twoDPoint.get().getX()),
                    new BigDecimal(twoDPoint.get().getY()), zAxis.get().getWordPair().getSecond().toString()));
        }

        return Optional.empty();
    }

    public Optional<Point3D> getReal3DPoint()
    {
        Optional<ThreeDPoint> threeDPointOpt = get3DPoint();

        if (threeDPointOpt.isPresent()) {
            ThreeDPoint threeDPoint = threeDPointOpt.get();
            String zValue = parameters.containsKey(threeDPoint.getZValue()) ?
                    parameters.get(threeDPoint.getZValue()).toPlainString() : threeDPoint.getZValue();

            if (isNumber(zValue)){
                return Optional.of(new Point3D(threeDPoint.getXValue().doubleValue(),
                        threeDPoint.getYValue().doubleValue(), Double.parseDouble(threeDPoint.getZValue())));
            }
        }

        return Optional.empty();
    }

    /**
     * @return All words that are modal.
     */
    public List<Word> getModalWords() {return ImmutableList.copyOf(modalWords.values());}

    /**
     * @param wordGroup The WordGroup that contains the Word you are searching for in this GCodeState
     * @return An Optional modal Word if it exists in the given WordGroup
     */
    public Optional<Word> getModalWordByGroup(WordGroup wordGroup)
    {
        return Optional.ofNullable(modalWords.get(wordGroup));
    }

    public List<Word> getCurrentWords() {return ImmutableList.copyOf(currentWords);}

    public Optional<GCodeState> getPreviousState() {return previousState;}

    public ImmutableMap<String, BigDecimal> getParameters(){return parameters;}

    public int getIteration(){return iteration;}

    public String getRawLine() {return rawLine;}

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GCodeState that = (GCodeState) o;

        if (iteration != that.iteration) return false;
        if (modalWords != null ? !modalWords.equals(that.modalWords) : that.modalWords != null) return false;
        if (currentWords != null ? !currentWords.equals(that.currentWords) : that.currentWords != null) return false;
        if (parameters != null ? !parameters.equals(that.parameters) : that.parameters != null) return false;
        return !(rawLine != null ? !rawLine.equals(that.rawLine) : that.rawLine != null);

    }

    @Override
    public int hashCode()
    {
        int result = modalWords != null ? modalWords.hashCode() : 0;
        result = 31 * result + (currentWords != null ? currentWords.hashCode() : 0);
        result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        result = 31 * result + iteration;
        result = 31 * result + (rawLine != null ? rawLine.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return String.format("GCodeState{iteration=%d, rawLine=%s", iteration, rawLine);
    }

    /**
     * Gets the word on current line denoted by the wordChar
     * This method is in place because we may want the Word stating with the wordChar but not care what the value of
     * the Word is.
     * @param wordChar the character denoting the Word (in a properly formatted gcode line there
     * should be only 1 word
     * @return the Word stating with wordChar or Optional.empty if none exists
     */
    public Optional<Word> wordBeginningOnCurrentLine(char wordChar)
    {
        return currentWords
                .stream()
                .filter(word -> word.getWordPair().getFirst().equals(wordChar))
                .findFirst();
    }

    /**
     * Returns a function which obtains the value part of a word which lies on the current line starting with
     * the character passed to the function
     * @return The returned function returns an Optional on execution because the required value may not exist
     */
    public Function<Character, Optional<BigDecimal>> wordValueOnCurrentLineGetter()
    {
        return character -> {
            Optional<Word> word = wordBeginningOnCurrentLine(character);

            if (!word.isPresent()) {return Optional.empty();}
            else{
                return Optional.of(word.get().getWordPair().getSecond());
            }
        };
    }
}

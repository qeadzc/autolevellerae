package com.cncsoftwaretools.autoleveller;

import javafx.geometry.Point2D;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.List;

public class MeshSurroundingPointTest
{
    /*
     (0,3: x0, y30 z#512), (1,3: x10, y30 z#513), (2,3: x20, y30 z#514), (3,3: x30, y30 z#515)
     (0,2: x0, y20 z#508), (1,2: x10, y20 z#509), (2,2: x20, y20 z#510), (3,2: x30, y20 z#511)
     (0,1: x0, y10 z#504), (1,1: x10, y10 z#505), (2,1: x20, y10 z#506), (3,1: x30, y10 z#507)
     (0,0: x0, y0 z#500), (1,0: x10, y0 z#501), (2,0: x20, y0 z#502), (3,0: x30, y0 z#503)
     */
    private Mesh mesh;

    @Before
    public void setUp() throws Exception
    {
        mesh = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(30), BigDecimal.valueOf(30))
                .buildSpacing(10)
                .build();
    }

    @Test
    public void getPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("20.00000"), new BigDecimal("30.00000"), "#514");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("30.00000"), new BigDecimal("30.00000"), "#515");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("20.00000"), new BigDecimal("20.00000"), "#510");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("30.00000"), new BigDecimal("20.00000"), "#511");

        Point2D pointToFind = new Point2D(22.342, 29.543);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getLowerPointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("0.00000"), new BigDecimal("10.00000"), "#504");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("10.00000"), new BigDecimal("10.00000"), "#505");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("0.00000"), new BigDecimal("0.00000"), "#500");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("10.00000"), new BigDecimal("0.00000"), "#501");

        Point2D pointToFind = new Point2D(7.895, 9.743);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }

    @Test
    public void getOutsidePointsFromMesh()
    {
        ThreeDPoint expectedTLPoint = ThreeDPoint.createPoint(new BigDecimal("30.00000"), new BigDecimal("30.00000"), "#515");
        ThreeDPoint expectedTRPoint = ThreeDPoint.createPoint(new BigDecimal("30.00000"), new BigDecimal("30.00000"), "#515");
        ThreeDPoint expectedBLPoint = ThreeDPoint.createPoint(new BigDecimal("30.00000"), new BigDecimal("20.00000"), "#511");
        ThreeDPoint expectedBRPoint = ThreeDPoint.createPoint(new BigDecimal("30.00000"), new BigDecimal("20.00000"), "#511");

        Point2D pointToFind = new Point2D(84.342, 29.543);
        List<ThreeDPoint> surroundingPoints = mesh.getSurroundingPoints(pointToFind); //TL=0, TR=1, BL=2, BR=3

        assertThat(surroundingPoints.get(0), equalTo(expectedTLPoint));
        assertThat(surroundingPoints.get(1), equalTo(expectedTRPoint));
        assertThat(surroundingPoints.get(2), equalTo(expectedBLPoint));
        assertThat(surroundingPoints.get(3), equalTo(expectedBRPoint));
    }
}

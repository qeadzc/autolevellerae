package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.OGF;
import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import com.cncsoftwaretools.autoleveller.reader.processor.GCodeProcessor;
import com.cncsoftwaretools.autoleveller.util.GraphicsContextWrapper;
import com.google.common.collect.ImmutableList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Rectangle;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.cncsoftwaretools.autoleveller.matcher.CloseDoubleMatcher.isDblArrayCloseTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.*;

public class OGFCanvasTest
{
    private GraphicsContextWrapper gcMock;
    private OGFCanvas canvas;
    private StringListReader stringListReader;
    private OGF ogfMock;
    private Canvas canvasMock;

    private ArgumentCaptor<double[]> xArgs;
    private ArgumentCaptor<double[]> yArgs;
    private ArgumentCaptor<Integer> pointCount;

    private ArgumentCaptor<Double> arcArg;
    private ArgumentCaptor<ArcType> arcTypeArg;

    @Before
    public void setUp() throws Exception
    {
        ogfMock = mock(OGF.class);
        gcMock = mock(GraphicsContextWrapper.class);
        canvasMock = mock(Canvas.class);
        stringListReader = new StringListReader();
        when(ogfMock.getSize()).thenReturn(new Rectangle());
        xArgs = ArgumentCaptor.forClass(double[].class);
        yArgs = ArgumentCaptor.forClass(double[].class);
        pointCount = ArgumentCaptor.forClass(Integer.class);
        arcArg = ArgumentCaptor.forClass(Double.class);
        arcTypeArg = ArgumentCaptor.forClass(ArcType.class);
    }

    @Test
    public void blankOGFAndNoDrawingInstructions_shouldBeEmptyDrawingInstructionList() throws IOException
    {
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, never()).strokePolyline(any(), any(), anyInt());
    }

    @Test
    public void singleLineOfLinearGCode_shouldNotResultInALineBecauseCoordEqualsPointNotLine() throws IOException
    {
        stringListReader.setStringList(ImmutableList.of("N4430G1X117.168Y121.277Z-1"));
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, never()).strokePolyline(any(), any(), anyInt());
    }

    @Test
    public void twoLinesOfLinearGCode_shouldBeFunctionWithSinglePolyLineWithSameCoords() throws IOException
    {
        stringListReader.setStringList(ImmutableList.of("G01 X161.2392Y4.6201Z-1", "G01 X161.2301Y4.6699Z-1"));
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, times(1)).strokePolyline(new double[]{161.2392, 161.2301},
                new double[]{-4.6201, -4.6699}, 2);
    }

    @Test
    public void singleLineOfArcGCode_shouldNotResultInAnArcBecauseNoPreviousPoint() throws IOException
    {
        stringListReader.setStringList(ImmutableList.of("N63360G2X154.430Y152.399Z-1I-0.784J23.274 F500"));
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, never()).strokeArc(anyDouble(), anyDouble(), anyDouble(), anyDouble(), anyDouble(), anyDouble(), any());
    }

    @Test
    public void twoLinesOfArcGCode_shouldBeFunctionWithSingleStrokeArcWithSameCoords() throws IOException
    {
        stringListReader.setStringList(ImmutableList.of("N63360G2X154.430Y152.399Z-1I-0.784J23.274 F500", "N63370G2X151.982Y152.664I2.051J30.421"));
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        //double radius = 30.48975790326975;
        //Point2D centre = Point2D [x = 156.481, y = 182.82];

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, times(1)).strokeArc(arcArg.capture(), arcArg.capture(), arcArg.capture(),
                arcArg.capture(), arcArg.capture(), arcArg.capture(), arcTypeArg.capture());

        assertThat(arcArg.getAllValues().get(0), closeTo(125.9912421, .0001));
        assertThat(arcArg.getAllValues().get(1), closeTo(-213.309756, .0001));
        assertThat(arcArg.getAllValues().get(2), closeTo(60.979514, .0001));
        assertThat(arcArg.getAllValues().get(3), closeTo(60.979514, .0001));
        assertThat(arcArg.getAllValues().get(4), closeTo(261.514578, .0001));
        assertThat(arcArg.getAllValues().get(5), closeTo(4.628345, .0001));
        assertThat(arcTypeArg.getAllValues().get(0), equalTo(ArcType.OPEN));
    }

    @Test
    public void threeLinesOfGCode_shouldResultInAPolyLineWithThreePoints() throws IOException
    {
        stringListReader.setStringList(ImmutableList.of("G01 X161.2392Y4.6201Z-1", "G01 X161.2301Y4.6699", "G01 X161.2197Y4.7190"));
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, times(1)).strokePolyline(new double[]{161.2392, 161.2301, 161.2197},
                new double[]{-4.6201, -4.6699, -4.7190}, 3);
    }

    @Test
    public void gCodeLinesSeparatedByArcLines_shouldResultIn2PolyLinewCalls()
    {
        stringListReader.setStringList(ImmutableList.of("N63330G1X162.244Y157.417Z-0.1", "N63340G1X159.130Y152.399",
                "N63350G3X156.780Y152.360I0.000J-69.836", "N63360G2X154.430Y152.399I-0.784J23.274", "N63370G2X151.982Y152.664I2.051J30.421",
                "N63380G3X142.231Y154.052I-107.940J-723.475", "N63390G1X135.180Y153.479", "N63400G1X136.577Y151.192", "N63410G1X133.719Y147.761"));
        canvas = new OGFCanvas(ogfMock, stringListReader, canvasMock);

        canvas.getDrawingInstructions().forEach(drawingFunc -> drawingFunc.apply(gcMock, 1.0, Point2D.ZERO));

        verify(gcMock, times(2)).strokePolyline(xArgs.capture(), yArgs.capture(), pointCount.capture());

        assertThat(xArgs.getAllValues().get(0), isDblArrayCloseTo(new double[]{162.244, 159.130}));
        assertThat(yArgs.getAllValues().get(0), isDblArrayCloseTo(new double[]{-157.417, -152.399}));
        assertThat(pointCount.getAllValues().get(0), equalTo(2));
        assertThat(xArgs.getAllValues().get(1), isDblArrayCloseTo(new double[]{142.231, 135.180, 136.577, 133.719}));
        assertThat(yArgs.getAllValues().get(1), isDblArrayCloseTo(new double[]{-154.052, -153.479, -151.192, -147.761}));
        assertThat(pointCount.getAllValues().get(1), equalTo(4));
    }

    private class StringListReader implements GCodeReader
    {
        private List<String> stringList = ImmutableList.of();

        void setStringList(List<String> stringList)
        {
            this.stringList = stringList;
        }

        @Override
        public void readFile(GCodeProcessor processor) throws IOException
        {
            final Optional[] previousState = new Optional[]{Optional.empty()};

            stringList.forEach(s -> {
                GCodeState currentState = GCodeState.newInstance(previousState[0], s);
                processor.interpret(ImmutableList.of(currentState));
                previousState[0] = Optional.of(currentState);
            });

            finishProcessing(processor);
        }

        // a recursive method to call finish() and log it on each processor from the bottom up
        private void finishProcessing(GCodeProcessor processor)
        {
            if (processor.nextProcessor().isPresent()){
                finishProcessing(processor.nextProcessor().get());
            }

            processor.finish();
        }
    }
}
package com.cncsoftwaretools.autoleveller.ui.controllers;

import com.cncsoftwaretools.autoleveller.*;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.testfx.framework.junit.ApplicationTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.testfx.api.FxToolkit.*;


@Ignore("Reason: TestxFX tests are difficult to run on a CI server. Ignoring all TestFX tests.")
@RunWith(MockitoJUnitRunner.class)
public class MeshTabControllerTestIT extends ApplicationTest
{
    //For a hi-res screen that has been scaled, enter the OS scale as a VM option SCALE
    //eg. add -DSCALE=200 if your OS uses a 200% scale to the command line
    private static double SCALEDPERCENT;

    @Mock
    private OGF ogfMock;

    private Node meshTab;
    private TextField startingXTxt;
    private TextField startingYTxt;
    private TextField widthTxt;
    private TextField heightTxt;
    private TextField xyFeedTxt;
    private TextField zFeedTxt;
    private TextField probeDepthTxt;
    private TextField probeClearanceTxt;
    private TextField probeSpacing;
    private TextField safeHeightTxt;
    private ChoiceBox<Units> unitsChc;
    private ChoiceBox<Controller> controllerChc;

    private final ALModel model = ALModel.getInstance();
    private final Autoleveller app = new Autoleveller();

    @Override
    public void start(Stage stage) throws Exception
    {
        app.start(stage);
    }

    @BeforeClass
    public static void setupSpec() throws Exception
    {
        registerPrimaryStage();
        setupStage(Stage::show);
        String SCALE = System.getProperty("SCALE");

        SCALEDPERCENT = SCALE != null ? Double.parseDouble(SCALE) : 100;
    }

    @Before
    public void setup() throws Exception
    {
        setupApplication(Autoleveller.class);

        unitsChc = this.lookup("#unitsChc").queryFirst();
        controllerChc = this.lookup("#controllerChc").queryFirst();
        meshTab = this.lookup("#meshTab").queryFirst();
        startingXTxt = this.lookup("#startingXTxt").queryFirst();
        startingYTxt = this.lookup("#startingYTxt").queryFirst();
        widthTxt = this.lookup("#xLengthTxt").queryFirst();
        heightTxt = this.lookup("#yLengthTxt").queryFirst();
        xyFeedTxt = this.lookup("#xyFeedTxt").queryFirst();
        zFeedTxt = this.lookup("#zFeedTxt").queryFirst();
        probeDepthTxt = this.lookup("#pDepthTxt").queryFirst();
        probeClearanceTxt = this.lookup("#pClearanceTxt").queryFirst();
        probeSpacing = this.lookup("#pSpacingTxt").queryFirst();
        safeHeightTxt = this.lookup("#pSafeHeightTxt").queryFirst();

        Rectangle size = new Rectangle(0, 0, 100, 60);
        when(ogfMock.getSize()).thenReturn(size);
        when(ogfMock.displayDetails()).thenReturn("X=" + size.getX() + " Y=" + size.getY() + " Width=" + size.getWidth() + " Height=" + size.getHeight());
        when(ogfMock.getUnits()).thenReturn(Units.MM);

        runAndWait(() -> {
            unitsChc.getSelectionModel().select(Units.MM);
            model.setMesh(null);
            model.setOGF(null);
        });
    }

    @Test
    public void changingUnitsInChoiceBox_shouldChangeModelUnits() throws ExecutionException, InterruptedException
    {
        runAndWait(() -> {
            ChoiceBox<Units> unitsChc = this.lookup("#unitsChc").queryFirst();
            unitsChc.getSelectionModel().select(Units.INCHES);
        });

        assertThat(unitsChc.getValue(), equalTo(Units.INCHES));
        assertThat(model.readUnits(), equalTo(Units.INCHES));

        //back again
        runAndWait(() -> unitsChc.getSelectionModel().select(Units.MM));

        assertThat(unitsChc.getValue(), equalTo(Units.MM));
        assertThat(model.readUnits(), equalTo(Units.MM));
    }

    @Test
    public void changeSegLengthInGUIShouldChangeSegLengthInModel() throws ExecutionException, InterruptedException
    {
        runAndWait(() -> {
            unitsChc.getSelectionModel().select(Units.INCHES);
            model.writeDouble(ALModel.MAX_SEG_LTH, 0.187);
        });
        showOptionsDlg("Set Maximum Segment Length");
        Spinner<Double> mslSpn = lookup("#mslSpn").queryFirst();
        Button okButton = lookup("#okBtn").queryFirst();
        assertThat(mslSpn.getValue(), equalTo(0.187));
        runAndWait(mslSpn::decrement);
        runAndWait(mslSpn::decrement);
        runAndWait(okButton::fire);
        sleep(100);

        assertThat(model.readDouble(ALModel.MAX_SEG_LTH), equalTo(0.177));

        runAndWait(() -> {
            unitsChc.getSelectionModel().select(Units.MM);
            model.writeDouble(ALModel.MAX_SEG_LTH, 6);
        });
        showOptionsDlg("Set Maximum Segment Length");
        mslSpn = lookup("#mslSpn").queryFirst();
        okButton = lookup("#okBtn").queryFirst();
        assertThat(mslSpn.getValue(), equalTo(6.0));
        runAndWait(mslSpn::increment);
        runAndWait(mslSpn::increment);
        runAndWait(okButton::fire);
        sleep(100);

        assertThat(model.readDouble(ALModel.MAX_SEG_LTH), equalTo(8.0));
    }

    private void showOptionsDlg(String menuOption) throws ExecutionException, InterruptedException
    {
        runAndWait(() -> clickOn("Options", MouseButton.PRIMARY));
        runAndWait(() -> sleep(1000));
        runAndWait(() -> clickOn(menuOption, MouseButton.PRIMARY));
        sleep(1000);
    }

    @Test
    public void changeControllerInGUI_shouldChangeInModel() throws ExecutionException, InterruptedException
    {
        controllerChc = this.lookup("#controllerChc").queryFirst();

        runAndWait(() -> controllerChc.getSelectionModel().select(Controller.MACH3));

        assertThat(controllerChc.getValue(), equalTo(Controller.MACH3));
        assertThat(model.readController(), equalTo(Controller.MACH3));

        runAndWait(() -> controllerChc.getSelectionModel().select(Controller.TURBOCNC));

        assertThat(controllerChc.getValue(), equalTo(Controller.TURBOCNC));
        assertThat(model.readController(), equalTo(Controller.TURBOCNC));

        runAndWait(() -> controllerChc.getSelectionModel().select(Controller.LINUXCNC));

        assertThat(controllerChc.getValue(), equalTo(Controller.LINUXCNC));
        assertThat(model.readController(), equalTo(Controller.LINUXCNC));
    }

    @Test
    public void whenEnteringNegativeHeightForMesh_modelShouldBeNullAndHeightShouldBeEmpty() throws ExecutionException, InterruptedException
    {
    	runAndWait(() -> {
            unitsChc.getSelectionModel().select(Units.MM);
            controllerChc.getSelectionModel().select(Controller.LINUXCNC);
        });

        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(meshTab, SCALEDPERCENT))
                .clickOn(MouseButton.PRIMARY);
        startingXTxt.setText("3.0000009");
        startingYTxt.setText("65.789");
        widthTxt.setText("100");
        heightTxt.setText("-88.6");
        xyFeedTxt.setText("250");
        zFeedTxt.setText("150");
        probeDepthTxt.setText("-1");
        probeClearanceTxt.setText("2");
        probeSpacing.setText("10");
        safeHeightTxt.setText("100.5");
        clickOn(AutolevellerUtil.getScaledClickPointForNode(startingXTxt, SCALEDPERCENT))
                .clickOn(MouseButton.PRIMARY)
                .press(KeyCode.TAB);

        assertThat(startingXTxt.getText(), equalTo("3.0000009"));
        assertThat(startingYTxt.getText(), equalTo("65.789"));
        assertThat(widthTxt.getText(), equalTo("100"));
        assertThat(heightTxt.getText(), equalTo(""));
        assertThat(xyFeedTxt.getText(), equalTo("250"));
        assertThat(zFeedTxt.getText(), equalTo("150"));
        assertThat(probeDepthTxt.getText(), equalTo("-1"));
        assertThat(probeClearanceTxt.getText(), equalTo("2"));
        assertThat(probeSpacing.getText(), equalTo("10"));
        assertThat(safeHeightTxt.getText(), equalTo("100.5"));

        assertThat(model.getMesh(), is(nullValue()));
    }

    @Test
    public void whenEnteringValidValesForMeshRequiredFields_modelMeshShouldBeUpdatedWithTheseValues() throws ExecutionException, InterruptedException
    {
        runAndWait(() -> {
            unitsChc.getSelectionModel().select(Units.INCHES);
            controllerChc.getSelectionModel().select(Controller.MACH3);
            //should only use these values if the entered value is invalid
            model.writeDouble(ALModel.XY_FEED_KEY, 30);
            model.writeDouble(ALModel.Z_FEED_KEY, 6.3);
            model.writeDouble(ALModel.PROBE_DEPTH, -0.34);
            model.writeDouble(ALModel.PROBE_CLEARANCE, 0.07);
            model.writeDouble(ALModel.PROBE_SPACING, 0.35);
            model.writeDouble(ALModel.SAFE_HEIGHT, 2.1);
        });
        sleep(100);

        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(meshTab, SCALEDPERCENT))
                .clickOn(MouseButton.PRIMARY);
        startingXTxt.setText("-4.6");
        startingYTxt.setText("0");
        widthTxt.setText("4.6");
        heightTxt.setText("5");
        xyFeedTxt.setText("50");
        zFeedTxt.setText("-40");
        probeDepthTxt.setText("-0.0375");
        probeClearanceTxt.setText("0.06");
        probeSpacing.setText("0.3");
        safeHeightTxt.setText("2");
        clickOn(AutolevellerUtil.getScaledClickPointForNode(startingXTxt, SCALEDPERCENT))
                .clickOn(MouseButton.PRIMARY)
                .press(KeyCode.TAB);

        assertThat(startingXTxt.getText(), equalTo("-4.6"));
        assertThat(startingYTxt.getText(), equalTo("0"));
        assertThat(widthTxt.getText(), equalTo("4.6"));
        assertThat(heightTxt.getText(), equalTo("5"));
        assertThat(xyFeedTxt.getText(), equalTo("50"));
        assertThat(zFeedTxt.getText(), equalTo("6.3"));
        assertThat(probeDepthTxt.getText(), equalTo("-0.0375"));
        assertThat(probeClearanceTxt.getText(), equalTo("0.06"));
        assertThat(probeSpacing.getText(), equalTo("0.3"));
        assertThat(safeHeightTxt.getText(), equalTo("2"));

        Mesh mesh = model.getMesh();
        assertThat(mesh.getxValue().doubleValue(), is(-4.6));
        assertThat(mesh.getyValue().doubleValue(), is(0.0));
        assertThat(mesh.getxLength().doubleValue(), is(4.6));
        assertThat(mesh.getyLength().doubleValue(), is(5.0));
        assertThat(mesh.getXyFeed().doubleValue(), is(50.0));
        assertThat(mesh.getzFeed().doubleValue(), is(6.3));
        assertThat(mesh.getProbeDepth().doubleValue(), is(-0.0375));
        assertThat(mesh.getProbeClearance().doubleValue(), is(0.06));
        assertThat(mesh.getPointSpacing().doubleValue(), is(0.3));
        assertThat(mesh.getSafeHeight().doubleValue(), is(2.0));
    }

    @Test
    public void givenValidOGFMock_jobRectangleWidthIsTheSameWidthAsPane() throws ExecutionException, InterruptedException
    {
        Pane pane = this.lookup("#probeLayoutPnl").queryFirst();
        Rectangle jobRect = this.lookup("#displayedOGFRect").queryFirst();

        runAndWait(() -> model.setOGF(ogfMock));
        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(meshTab, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY);

        BigDecimal scaledWidth = new BigDecimal(jobRect.getLayoutBounds().getWidth()).setScale(5, RoundingMode.HALF_EVEN);
        assertThat(scaledWidth, equalTo(new BigDecimal(pane.getWidth()).setScale(5, RoundingMode.HALF_EVEN)));
    }

    @Test
    public void afterGUIConstructionOfMesh_paneWidthEqualsDisplayedMeshWidth() throws ExecutionException, InterruptedException
    {
        Pane pane = this.lookup("#probeLayoutPnl").queryFirst();
        Rectangle meshRect = this.lookup("#displayedMeshRect").queryFirst();

        runAndWait(() -> model.setOGF(null));
        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(meshTab, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY);
        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(startingXTxt, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY)
                .type(KeyCode.DIGIT0)
                .clickOn(AutolevellerUtil.getScaledClickPointForNode(startingYTxt, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY)
                .type(KeyCode.DIGIT0)
                .clickOn(AutolevellerUtil.getScaledClickPointForNode(widthTxt, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY)
                .type(KeyCode.DIGIT1, KeyCode.DIGIT0, KeyCode.DIGIT1)
                .clickOn(AutolevellerUtil.getScaledClickPointForNode(heightTxt, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY)
                .type(KeyCode.DIGIT5, KeyCode.DIGIT2, KeyCode.ENTER);

        assertThat(meshRect.getWidth(), equalTo(pane.getWidth()));
    }

    @Test
    public void givenAnOGFAndConstructingAValidMesh_GUIRectanglesAreDisplayedCorrectlyGivenMeshsTakePriorityOnThisTab() throws ExecutionException, InterruptedException
    {
        Pane pane = this.lookup("#probeLayoutPnl").queryFirst();
        Rectangle jobRect = this.lookup("#displayedOGFRect").queryFirst();
        Rectangle meshRect = this.lookup("#displayedMeshRect").queryFirst();

        runAndWait(() -> model.setOGF(ogfMock));
        runAndWait(() -> model.writeDouble(ALModel.PROBE_SPACING, 10));
        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(meshTab, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY);
        startingXTxt.setText("");
        startingYTxt.setText("");
        widthTxt.setText("");
        heightTxt.setText("");
        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(startingXTxt, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY)
                .type(KeyCode.MINUS, KeyCode.DIGIT5, KeyCode.TAB)
                .type(KeyCode.MINUS, KeyCode.DIGIT1, KeyCode.DIGIT0, KeyCode.TAB)
                .type(KeyCode.DIGIT1, KeyCode.DIGIT0, KeyCode.DIGIT0, KeyCode.TAB)
                .type(KeyCode.DIGIT5, KeyCode.DIGIT2, KeyCode.ENTER);

        double sizeMultiplier = pane.getWidth() / 100;
        double modelOgfX = model.getOGF().getSize().getX();
        double modelOgfY = model.getOGF().getSize().getY();
        double modelMeshX = model.getMesh().getSize().getX();
        double modelMeshY = model.getMesh().getSize().getY();

        double jobXPosOffset = (modelOgfX - modelMeshX) * sizeMultiplier;
        BigDecimal realYPos = new BigDecimal((meshRect.getLayoutY() + meshRect.getHeight()) -
                (jobRect.getLayoutY() + jobRect.getHeight())).setScale(5, RoundingMode.HALF_EVEN);
        BigDecimal calculatdYPos = new BigDecimal((modelOgfY - modelMeshY) * sizeMultiplier).setScale(5, RoundingMode.HALF_EVEN);

        assertThat(jobRect.getLayoutX(), equalTo(jobXPosOffset));
        assertThat(realYPos, equalTo(calculatdYPos));
    }

    @Test
    public void switchingUnits_shouldSetAppropriateMeshPrompts() throws ExecutionException, InterruptedException
    {
        final DecimalFormat df = Autoleveller.DF;
        final DefaultMeshValues mmDefaults = DefaultMeshValues.MM;
        final DefaultMeshValues inDefaults = DefaultMeshValues.INCHES;

        this.clickOn(AutolevellerUtil.getScaledClickPointForNode(meshTab, SCALEDPERCENT)).clickOn(MouseButton.PRIMARY);
        runAndWait(() -> unitsChc.getSelectionModel().select(Units.MM));

        assertThat(xyFeedTxt.getPromptText(), equalTo(df.format(mmDefaults.getXYFeed())));
        assertThat(zFeedTxt.getPromptText(), equalTo(df.format(mmDefaults.getZFeed())));
        assertThat(probeDepthTxt.getPromptText(), equalTo(df.format(mmDefaults.getProbeDepth())));
        assertThat(probeClearanceTxt.getPromptText(), equalTo(df.format(mmDefaults.getProbeClearance())));
        assertThat(probeSpacing.getPromptText(), equalTo(df.format(mmDefaults.getPointSpacing())));
        assertThat(safeHeightTxt.getPromptText(), equalTo(df.format(mmDefaults.getSafeHeight())));

        runAndWait(() -> unitsChc.getSelectionModel().select(Units.INCHES));

        assertThat(xyFeedTxt.getPromptText(), equalTo(df.format(inDefaults.getXYFeed())));
        assertThat(zFeedTxt.getPromptText(), equalTo(df.format(inDefaults.getZFeed())));
        assertThat(probeDepthTxt.getPromptText(), equalTo(df.format(inDefaults.getProbeDepth())));
        assertThat(probeClearanceTxt.getPromptText(), equalTo(df.format(inDefaults.getProbeClearance())));
        assertThat(probeSpacing.getPromptText(), equalTo(df.format(inDefaults.getPointSpacing())));
        assertThat(safeHeightTxt.getPromptText(), equalTo(df.format(inDefaults.getSafeHeight())));
    }

    @Test
    public void givenStartingYWithInvalidAndValidText_shouldReturnAsAppropriate()
    {
        MeshTabController mtc = app.getRootController().meshContentController;

        startingYTxt.setText("0");
        assertThat(mtc.isValidField(startingYTxt), is(true));

        startingYTxt.setText("-200");
        assertThat(mtc.isValidField(startingYTxt), is(true));

        startingYTxt.setText("not a number");
        assertThat(mtc.isValidField(startingYTxt), is(false));
    }

    @Test
    public void givenXYFeedWithInvalidAndValidText_shouldReturnAsAppropriate()
    {
        MeshTabController mtc = app.getRootController().meshContentController;

        xyFeedTxt.setText("40");
        assertThat(mtc.isValidField(xyFeedTxt), is(true));

        xyFeedTxt.setText("-200");
        assertThat(mtc.isValidField(xyFeedTxt), is(false));

        xyFeedTxt.setText("not a number");
        assertThat(mtc.isValidField(xyFeedTxt), is(false));
    }

    @Test
    public void givenDepthWithInvalidAndValidText_shouldReturnAsAppropriate()
    {
        MeshTabController mtc = app.getRootController().meshContentController;

        probeDepthTxt.setText("40");
        assertThat(mtc.isValidField(probeDepthTxt), is(false));

        probeDepthTxt.setText("-0.3");
        assertThat(mtc.isValidField(probeDepthTxt), is(true));

        probeDepthTxt.setText("not a number");
        assertThat(mtc.isValidField(probeDepthTxt), is(false));
    }

    @FunctionalInterface
    private interface Procedure
    {
        void accept();
    }

    private void runAndWait(Procedure procedure) throws ExecutionException, InterruptedException
    {
        CompletableFuture<String> future = new CompletableFuture<>();
        Platform.runLater(() -> {
                    procedure.accept();
                    future.complete("done");
                });
        future.get();
    }
}
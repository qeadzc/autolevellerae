package com.cncsoftwaretools.autoleveller.ui;

import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class PreferenceImplTestIT
{
    @Test
    public void badPath_shouldReturnTheValueSetButValueIsNotPersisted()
    {

        Path badPath = Paths.get("/disk/%%%%");

        Preferences pref1 = PreferenceImpl.newInstance(badPath);
        pref1.set("key", "val");
        Preferences pref2 = PreferenceImpl.newInstance(badPath);

        assertThat(Files.exists(badPath), is(false));
        assertThat(pref1.get("key"), equalTo(Optional.of("val")));
        assertThat(pref2.get("key"), equalTo(Optional.empty()));
    }

    @Test
    public void givenAValidPath_shouldBeAbleToRetrieveValueSetFromNewInstance()
    {
        Path goodPath = Paths.get(System.getProperty("user.home"), "testtemp.xml");

        Preferences pref1 = PreferenceImpl.newInstance(goodPath);
        pref1.set("key", "val");
        Preferences pref2 = PreferenceImpl.newInstance(goodPath);

        assertThat(pref1.get("key"), equalTo(Optional.of("val")));
        assertThat(pref2.get("key"), equalTo(Optional.of("val")));
    }

    @Test
    public void unwrittenKeyValuePair_shouldReturnEmptyOptional()
    {
        Path path = Paths.get(System.getProperty("user.home"), "testtemp.xml");

        Preferences pref = PreferenceImpl.newInstance(path);

        assertThat(pref.get("unwritten"), equalTo(Optional.empty()));
    }

    @Test
    public void writingMultipleKeyValuePairs_shouldAllBeReadLater()
    {
        Path path = Paths.get(System.getProperty("user.home"), "testtemp.xml");

        Preferences prefWriter = PreferenceImpl.newInstance(path);
        prefWriter.set("Joe", "Bloggs");
        prefWriter.set("Lou", "Reed");
        prefWriter.set("David", "Beckham");
        prefWriter.set("Henry", "Winkler");
        prefWriter.set("Del", "Boy");
        Preferences prefReader = PreferenceImpl.newInstance(path);

        assertThat(prefReader.get("Joe"), equalTo(Optional.of("Bloggs")));
        assertThat(prefReader.get("Lou"), equalTo(Optional.of("Reed")));
        assertThat(prefReader.get("David"), equalTo(Optional.of("Beckham")));
        assertThat(prefReader.get("Henry"), equalTo(Optional.of("Winkler")));
        assertThat(prefReader.get("Del"), equalTo(Optional.of("Boy")));
    }
}
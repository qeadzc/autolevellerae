package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.PlainGCodeReader;
import com.cncsoftwaretools.autoleveller.reader.processor.*;
import com.cncsoftwaretools.autoleveller.ui.ALModel;
import com.google.common.collect.ImmutableList;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

/**
 * These tests interpret all gcode files in the folder constant.
 * The outputs are processed files which should be opened in a cam program for visualizing.
 * These tests are slow, contain no assertians and are purely for visualizing and therefore should
 * be '@ignore'ed predominantly.
 */
@Ignore
public class ProcessingVisualTestsIT
{
    private static final String gcodeFolder = "D:\\Google Drive\\Autoleveller\\TestGcode";

    @Test
    public void segmentFiles() throws IOException
    {
        List<File> files = ImmutableList.copyOf(new File(gcodeFolder).listFiles(File::isFile));

        files.stream().forEach(path -> {
            try {
                GCodeReader reader = PlainGCodeReader.newInstance(new FileReader(path.toString()));
                String fileOutput = Paths.get(gcodeFolder, "segmented", "Segmented" + path.getName()).toString();
                GCodeProcessor writer = WriterProcessor.newInstance(Optional.<GCodeProcessor>empty(), fileOutput, false);
                GCodeProcessor arcSegmentor = ArcSegmentProcessor.newInstance(Optional.of(writer), 5);
                GCodeProcessor segmentor = LineSegmentProcessor.newInstance(Optional.of(arcSegmentor), 5);
                reader.readFile(segmentor);
                System.out.println(fileOutput);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void levelledFiles() throws IOException
    {
        List<File> files = ImmutableList.copyOf(new File(gcodeFolder).listFiles(File::isFile));

        files.stream().forEach(path -> {
            try {
                System.out.println(path.toString());
                Optional<OGF> ogf = OGF.newInstance(path.toString());

                Mesh mesh = new Mesh.Builder(ogf.get().getUnits(), Controller.LINUXCNC,
                        BigDecimal.valueOf(ogf.get().getSize().getX()), BigDecimal.valueOf(ogf.get().getSize().getY()),
                        BigDecimal.valueOf(ogf.get().getSize().getWidth()), BigDecimal.valueOf(ogf.get().getSize().getHeight()))
                        .build();

                GCodeReader reader = PlainGCodeReader.newInstance(new FileReader(path.toString()));
                String fileOutput = Paths.get(gcodeFolder, "levelled", "AL" + path.getName()).toString();
                GCodeProcessor writer = WriterProcessor.newInstance(Optional.<GCodeProcessor>empty(), fileOutput, false);
                GCodeProcessor leveller = LevelerProcessor.newInstance(Optional.of(writer), mesh);
                GCodeProcessor arcSegmentor = ArcSegmentProcessor.newInstance(Optional.of(leveller), 5);
                GCodeProcessor segmentor = LineSegmentProcessor.newInstance(Optional.of(arcSegmentor), 5);
                reader.readFile(segmentor);
                System.out.println(fileOutput);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void fullLevelledFiles() throws IOException
    {
        List<File> files = ImmutableList.copyOf(new File(gcodeFolder).listFiles(File::isFile));

        files.stream().forEach(path -> {
            ALModel model = ALModel.getInstance();

            Optional<OGF> ogf = OGF.newInstance(path.toString());
            model.setOGF(ogf.get());
            model.setUnits(ogf.get().getUnits());

            Mesh mesh = new Mesh.Builder(ogf.get().getUnits(), model.readController(),
                    BigDecimal.valueOf(ogf.get().getSize().getX()), BigDecimal.valueOf(ogf.get().getSize().getY()),
                    BigDecimal.valueOf(ogf.get().getSize().getWidth()), BigDecimal.valueOf(ogf.get().getSize().getHeight()))
                    .build();
            model.setMesh(mesh);

            model.setUnits(ogf.get().getUnits());
            model.writeDouble(ALModel.MAX_SEG_LTH, getDefaultSegLength().doubleValue());
            model.writeController(mesh.getController());
            String outFilename =Paths.get(gcodeFolder, "full", "AL" + path.getName()).toString();

            mesh.writeLevelled(outFilename, model);
        });
    }

    @Test
    public void fullRPFLevelledFiles() throws IOException
    {
        List<File> files = ImmutableList.copyOf(new File(gcodeFolder).listFiles(File::isFile));

        files.stream().forEach(path -> {
            ALModel model = ALModel.getInstance();

            Optional<OGF> ogf = OGF.newInstance(path.toString());
            model.setOGF(ogf.get());
            model.setUnits(ogf.get().getUnits());
            Optional<RPF> rpf = RPF.newInstance("src/test/ALProbeEMCSmall.test");
            model.setRPF(rpf.get());

            model.writeDouble(ALModel.MAX_SEG_LTH, getDefaultSegLength().doubleValue());
            model.writeController(model.readController()); //????????????
            String outFilename =Paths.get(gcodeFolder, "full", "AL" + path.getName()).toString();

            rpf.get().writeLevelled(outFilename, model);
        });
    }

    private BigDecimal getDefaultSegLength()
    {
        ALModel model = ALModel.getInstance();

        return model.readUnits().equals(Units.MM) ? new BigDecimal("5") : new BigDecimal("0.187");
    }

}

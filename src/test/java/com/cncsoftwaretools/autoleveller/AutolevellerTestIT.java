package com.cncsoftwaretools.autoleveller;

import org.junit.Test;

import java.util.Optional;
import java.util.Properties;
import java.util.regex.Pattern;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class AutolevellerTestIT
{
    private final Autoleveller autoleveller = new Autoleveller();
    //pattern example 12.3.3
    private final Pattern versionPattern = Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{1,2}u\\d$");

    @Test
    public void getAPIVersion()
    {
        assertThat(Autoleveller.getProperties("/.properties"), is(not(Optional.<String>empty())));
        assertThat(Autoleveller.getProperties("/.nonexistant"), is(Optional.<String>empty()));
    }

    @Test
    public void getValidProperties()
    {
        assertThat(Autoleveller.getProperties("/.properties").isPresent(), is(true));
    }

    @Test
    public void getInvalidProperties()
    {
        assertThat(Autoleveller.getProperties("/.nonexistantproperties").isPresent(), is(false));
    }

    @Test
    public void getValidProperty()
    {
        Properties pFile = Autoleveller.getProperties("/.properties").get();
        String version = autoleveller.getProperty(pFile, "AutolevellerAE.version").get();

        assertThat(versionPattern.matcher(version).matches(), is(true));
    }

}
package com.cncsoftwaretools.autoleveller;

import javafx.scene.shape.Rectangle;
import org.junit.Test;

import java.math.BigDecimal;
import com.cncsoftwaretools.autoleveller.Mesh.Inset;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@SuppressWarnings("BigDecimalMethodWithoutRoundingCalled")
public class MeshTest
{
    @Test
    public void defaultMesh()
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.TURBOCNC, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(20), BigDecimal.valueOf(20))
                .build();

        assertThat(mesh.getRowCount(), equalTo(3));
        assertThat(mesh.getColCount(), equalTo(3));
    }

    @Test
    public void meshPoints()
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(20), BigDecimal.valueOf(20))
                .build();

        ThreeDPoint firstRowFirstCol = ThreeDPoint.createPoint(new BigDecimal("0.0").setScale(5),
                new BigDecimal("0.0").setScale(ALTextBlockFunctions.SCALE), "#500");
        ThreeDPoint secondRowSecondCol = ThreeDPoint.createPoint(new BigDecimal("10.0").setScale(5),
                new BigDecimal("10.0").setScale(ALTextBlockFunctions.SCALE), "#504");
        ThreeDPoint thirdRowThirdCol = ThreeDPoint.createPoint(new BigDecimal("20.0").setScale(5),
                new BigDecimal("20.0").setScale(ALTextBlockFunctions.SCALE), "#508");

        assertThat(mesh.getPoint(0, 0).get(), equalTo(firstRowFirstCol));
        assertThat(mesh.getPoint(1, 1).get(), equalTo(secondRowSecondCol));
        assertThat(mesh.getPoint(2, 2).get(), equalTo(thirdRowThirdCol));
    }

    @Test
    public void builderInputs()
    {
        Mesh mesh = new Mesh.Builder(Units.INCHES, Controller.LINUXCNC, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(5), BigDecimal.valueOf(3))
                .buildDepth(-0.05)
                .buildSpacing(0.5)
                .buildZFeed(7)
                .buildXYFeed(30)
                .buildClearance(0.8)
                .buildSafeHeight(3)
                .build();

        assertThat(mesh.getUnits(), equalTo(Units.INCHES));
        assertThat(mesh.getxValue(), equalTo(BigDecimal.ZERO.setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getyValue(), equalTo(BigDecimal.ZERO.setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getxLength(), equalTo(BigDecimal.valueOf(5).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getyLength(), equalTo(BigDecimal.valueOf(3).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getProbeDepth(), equalTo(BigDecimal.valueOf(-0.05).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getPointSpacing(), equalTo(BigDecimal.valueOf(0.5).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getzFeed(), equalTo(BigDecimal.valueOf(7.0).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getXyFeed(), equalTo(BigDecimal.valueOf(30.0).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getProbeClearance(), equalTo(BigDecimal.valueOf(0.8).setScale(ALTextBlockFunctions.SCALE)));
        assertThat(mesh.getSafeHeight(), equalTo(BigDecimal.valueOf(3.0).setScale(ALTextBlockFunctions.SCALE)));

        assertThat(mesh.getPoint(0, 0).get(), equalTo(ThreeDPoint.createPoint(BigDecimal.valueOf(
                0.00000).setScale(ALTextBlockFunctions.SCALE), BigDecimal.valueOf(0.00000).setScale(ALTextBlockFunctions.SCALE), "#500")));
        assertThat(mesh.getPoint(6, 10).get(), equalTo(ThreeDPoint.createPoint(BigDecimal.valueOf(
                5.00000).setScale(ALTextBlockFunctions.SCALE), BigDecimal.valueOf(3.00000).setScale(ALTextBlockFunctions.SCALE), "#576")));
    }

    @Test
    public void negativexyMesh()
    {
        Mesh mesh = new Mesh.Builder(Units.INCHES, Controller.LINUXCNC, BigDecimal.valueOf(-5.45), BigDecimal.valueOf(0),
                BigDecimal.valueOf(5.45), BigDecimal.valueOf(4.94))
                .buildSpacing(1)
                .build();

        assertThat(mesh.getPoint(0, 0).get(), equalTo(ThreeDPoint.createPoint(BigDecimal.valueOf(
                -5.45000).setScale(ALTextBlockFunctions.SCALE), BigDecimal.valueOf(0.00000).setScale(ALTextBlockFunctions.SCALE), "#500")));
        assertThat(mesh.getPoint(4, 5).get(), equalTo(ThreeDPoint.createPoint(BigDecimal.valueOf(
                0.00000).setScale(ALTextBlockFunctions.SCALE), BigDecimal.valueOf(4.94000).setScale(ALTextBlockFunctions.SCALE), "#529")));
        assertThat(mesh.toString(), allOf(containsString("xValue=-5.45000"), containsString("yValue=0.00000"),
                containsString("xLength=5.45000"), containsString("yLength=4.94000")));
    }

    @Test(expected =  IllegalArgumentException.class)
    public void buildBadInputs()
    {
        //length cannot be <= 0
        new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.valueOf(-111), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO)
                .buildSpacing(-10) //cannot be <=0
                .buildClearance(-3) //cannot be <=0
                .buildDepth(-2) //must be negative
                .buildXYFeed(-400) //cannot be <=0
                .buildZFeed(-34) //cannot be <=0
                .buildSafeHeight(-4) //cannot be <=0
                .build();

    }

    @Test
    public void noInsetsBuilt_meshInsetsShouldBeZero()
    {
        Mesh mesh = new Mesh.Builder(Units.INCHES, Controller.LINUXCNC, BigDecimal.valueOf(-5.45), BigDecimal.valueOf(0),
                BigDecimal.valueOf(5.45), BigDecimal.valueOf(4.94))
                .build();

        assertThat(mesh.getInset(Inset.LEFT), equalTo(0.0));
        assertThat(mesh.getInset(Inset.RIGHT), equalTo(0.0));
        assertThat(mesh.getInset(Inset.TOP), equalTo(0.0));
        assertThat(mesh.getInset(Inset.BOTTOM), equalTo(0.0));
    }

    @Test
    public void meshInsets_shouldEqualBuiltWithInsets()
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(20), BigDecimal.valueOf(20))
                .buildInsets(1, 2, 0, 4)
                .build();

        assertThat(mesh.getInset(Inset.LEFT), equalTo(1.0));
        assertThat(mesh.getInset(Inset.RIGHT), equalTo(2.0));
        assertThat(mesh.getInset(Inset.TOP), equalTo(0.0));
        assertThat(mesh.getInset(Inset.BOTTOM), equalTo(4.0));

        assertThat(mesh.getxValue().doubleValue(), equalTo(1.0));
        assertThat(mesh.getyValue().doubleValue(), equalTo(4.0));
        assertThat(mesh.getxLength().doubleValue(), equalTo(17.0)); //width - left inset - right inset
        assertThat(mesh.getyLength().doubleValue(), equalTo(16.0)); //height - bottom inset - top inset
    }

    @Test
    public void negativeInsets_shouldIncreaseMeshSize()
    {
        Mesh mesh = new Mesh.Builder(Units.INCHES, Controller.LINUXCNC, BigDecimal.valueOf(-5.45), BigDecimal.valueOf(0),
                BigDecimal.valueOf(5.45), BigDecimal.valueOf(4.94))
                .buildInsets(-0.24, -1.1, -0.43, -0.32)
                .build();

        assertThat(mesh.getxValue().doubleValue(), equalTo(-5.69));
        assertThat(mesh.getyValue().doubleValue(), equalTo(-0.32));
        assertThat(mesh.getxLength().doubleValue(), equalTo(6.79)); //width - left inset - right inset
        assertThat(mesh.getyLength().doubleValue(), equalTo(5.69)); //height - bottom inset - top inset
    }

    @Test
    public void insetsOverlap_meshShouldNotBeBuiltWithInsets()
    {
        try {
            Mesh mesh = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO,
                    BigDecimal.valueOf(20), BigDecimal.valueOf(20))
                    .buildInsets(11, 9, 0, 4)
                    .build();

            assertThat(mesh.getSize(), equalTo(new Rectangle(0, 0, 20, 20)));
            assertThat(mesh.getInset(Inset.LEFT), equalTo(0.0));
            assertThat(mesh.getInset(Inset.RIGHT), equalTo(0.0));
            assertThat(mesh.getInset(Inset.TOP), equalTo(0.0));
            assertThat(mesh.getInset(Inset.BOTTOM), equalTo(0.0));
        }
        catch (IllegalArgumentException iae) {
            //ignore exception thrown with overlapping insets
        }
    }

    @Test
    public void multipleInsetSettings_shouldBeAppliedInAdditionToCurrentInsets()
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.MACH3, BigDecimal.ZERO, BigDecimal.ZERO,
                BigDecimal.valueOf(100), BigDecimal.valueOf(100))
                .buildInsets(10, 20, 30, 40)
                .build();

        mesh = new Mesh.Builder(mesh.getUnits(), mesh.getController(),
                mesh.getInsetlessSize().get(Mesh.Rect.X), mesh.getInsetlessSize().get(Mesh.Rect.Y),
                mesh.getInsetlessSize().get(Mesh.Rect.WIDTH), mesh.getInsetlessSize().get(Mesh.Rect.HEIGHT))
                .buildInsets(10,0, 0, 10)
                .build();

        assertThat(mesh.getSize().toString(), equalTo(new Rectangle(10, 10, 90, 90).toString()));
    }
}


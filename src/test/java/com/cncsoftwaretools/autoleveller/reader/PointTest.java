package com.cncsoftwaretools.autoleveller.reader;

import javafx.geometry.Point3D;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class PointTest
{
    @Test
    public void get3DPointOnInitState_ShouldBeSameAsGCodeLine()
    {
        GCodeState initState = GCodeState.newInstance(Optional.empty(), "G1 X-47.8655 Y-27.9646 Z-0.147625");

        assertThat(initState.getReal3DPoint(), equalTo(Optional.of(new Point3D(-47.8655, -27.9646, -0.147625))));
    }

    @Test
    public void get3DPointWhenZIsAParmAndNoParmsExistInTheState_ShouldBeEmptyOptional()
    {
        GCodeState initState = GCodeState.newInstance(Optional.empty(), "G1 X-47.8655 Y-27.9646 Z#10");

        assertThat(initState.getReal3DPoint(), equalTo(Optional.empty()));
    }

    @Test
    public void get3DPointWhenZIsACalculation_ShouldBeEmptyOptional()
    {
        GCodeState initState = GCodeState.newInstance(Optional.empty(), "G1 X-47.8655 Y-27.9646 Z[2 * 5]");

        assertThat(initState.getReal3DPoint(), equalTo(Optional.empty()));
    }

    @Test
    public void get3DPointWhenZIsAParmAndTheParmExistInTheState_ShouldBeAValidPoint3D()
    {
        GCodeState initState = GCodeState.newInstance(Optional.empty(), "#10=3");
        GCodeState firstState = GCodeState.newInstance(Optional.of(initState), "G1 X1 Y2 Z#10");

        assertThat(firstState.getReal3DPoint(), equalTo(Optional.of(new Point3D(1,2, 3))));
    }

    @Test
    public void hash10ShouldBeReplacedWith5DuringParsingIfFollowedByEquals()
    {
        GCodeState state = GCodeState.newInstance(Optional.empty(), "#10=3");
        state = GCodeState.newInstance(Optional.of(state), "#10=5");
        state = GCodeState.newInstance(Optional.of(state), "G1 X1 Y2 Z#10");

        assertThat(state.getReal3DPoint(), equalTo(Optional.of(new Point3D(1,2,5))));
    }
}

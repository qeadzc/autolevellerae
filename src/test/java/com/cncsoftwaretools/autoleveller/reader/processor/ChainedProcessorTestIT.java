package com.cncsoftwaretools.autoleveller.reader.processor;

import com.cncsoftwaretools.autoleveller.Controller;
import com.cncsoftwaretools.autoleveller.Mesh;
import com.cncsoftwaretools.autoleveller.Units;
import com.cncsoftwaretools.autoleveller.reader.GCodeReader;
import com.cncsoftwaretools.autoleveller.reader.PlainGCodeReader;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ChainedProcessorTestIT
{
    private BufferedReader mockedFileReader;
    private GCodeReader reader;
    private GCodeProcessor segmentor;

    @Before
    public void setup() throws IOException
    {
        Mesh mesh = new Mesh.Builder(Units.MM, Controller.LINUXCNC, new BigDecimal("0"), new BigDecimal("0"),
                new BigDecimal("150"), new BigDecimal("150"))
                .buildSpacing(20)
                .build();

        mockedFileReader = mock(BufferedReader.class);

        reader = PlainGCodeReader.newInstance(mockedFileReader);

        GCodeProcessor realWriter = WriterProcessor.newInstance(Optional.<GCodeProcessor>empty(), "src/test/outFile.txt", false);
        GCodeProcessor leveller = LevelerProcessor.newInstance(Optional.of(realWriter), mesh);
        segmentor = LineSegmentProcessor.newInstance(Optional.of(leveller), 5);
    }

    @Test
    public void allChainedProcessorsUsed() throws IOException
    {
        when(mockedFileReader.readLine())
                .thenReturn("%")
                .thenReturn("N390G1X119.522Y128.735Z-0.1")
                .thenReturn("N400G1X119.522Y128.822")
                .thenReturn("N410G1Y139.287")
                .thenReturn("N420G1X115.867")
                .thenReturn("N430G1X115.876Y139.278")
                .thenReturn("N440G1X115.884Y139.268")
                .thenReturn("N450G1X115.893Y139.258")
                .thenReturn("N460G1X115.901Y139.248")
                .thenReturn(null);

        reader.readFile(segmentor);

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            List<String> lines = br.lines().collect(Collectors.toList());

            assertThat(lines, not(hasItem("%"))); //check % has been removed
            //check lines segmented as expected
            assertThat(lines, hasItem("N410G1Y133.82200Z[#100+-0.1] ;segmented line. Max segment length set to 5"));
            assertThat(lines, hasItem("N410G1Y138.82200Z[#100+-0.1] ;segmented line. Max segment length set to 5"));
            //check lines levelled as expected
            assertThat(lines, hasItem("#102=[#561+0.50129*#553-0.50129*#561]"));
            assertThat(lines, hasItem("#101=[#562+0.50129*#554-0.50129*#562]"));
            assertThat(lines, hasItem("#100=[#102+0.40834*#101-0.40834*#102]"));
            assertThat(lines, hasItem("N450G1X115.893Y139.258Z[#100+-0.1]"));
        }
    }

    @Test
    public void levellerProcessorChained() throws IOException
    {
        when(mockedFileReader.readLine())
                .thenReturn("G1 F60.0 X46.7548 Y10.6213 Z-0.0161")
                .thenReturn("G1 X46.7153 Y10.5817 Z-0.0162")
                .thenReturn("G1 X46.7548 Y10.6213 Z-0.0163")
                .thenReturn("G1 X47.2348 Y10.6223 Z-0.0172")
                .thenReturn("G1 X47.2744 Y10.5829 Z-0.0173")
                .thenReturn(null);

        reader.readFile(segmentor);

        try(BufferedReader br=new BufferedReader(new FileReader("src/test/outFile.txt"))){
            List<String> lines = br.lines().collect(Collectors.toList());

            //check first levelling lines
            assertThat(lines.get(0), equalTo("#102=[#510+0.50434*#502-0.50434*#510]"));
            assertThat(lines.get(1), equalTo("#101=[#511+0.50434*#503-0.50434*#511]"));
            assertThat(lines.get(2), equalTo("#100=[#102+0.18189*#101-0.18189*#102]"));
            assertThat(lines.get(3), equalTo("G1 F60.0 X46.7548 Y10.6213 Z[#100+-0.0161]"));
            //check line count; should be 4 per input
            assertThat(lines.size(), is(20));
        }
    }
}

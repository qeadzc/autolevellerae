package com.cncsoftwaretools.autoleveller.reader;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasEntry;

public class StateTest
{
    @Test
    public void getFeed()
    {
        GCodeState state = GCodeState.newInstance(Optional.<GCodeState>empty(), "N10 F200");

        Word feedWord = state.getModalWordByGroup(WordGroup.FEEDGROUP).get();

        assertThat(feedWord, equalTo(Word.<Character, BigDecimal>createPair('F', BigDecimal.valueOf(200))));
        assertThat(feedWord.getDescription(), equalTo("Feed rate"));
    }

    @Test
    public void getEmptyMotionGroup()
    {
        GCodeState state = GCodeState.newInstance(Optional.empty(), "N10 F200");

        assertThat(state.getModalWordByGroup(WordGroup.MOTIONGROUP), equalTo(Optional.<Word>empty()));
    }

    @Test
    public void getUniqueMotion()
    {
        GCodeState state = GCodeState.newInstance(Optional.empty(), "N10 G00 G1 G2 G3");

        assertThat(state.getCurrentWords().size(), equalTo(5));
        assertThat(state.getModalWords().size(), equalTo(1));
        assertThat(state.getModalWordByGroup(WordGroup.MOTIONGROUP).get(), equalTo(Word.createPair('G', BigDecimal.valueOf(3))));
    }

    @Test
    public void getParameter()
    {
        GCodeState state = GCodeState.newInstance(Optional.empty(), "N10 F200");

        state = GCodeState.newInstance(Optional.of(state), "#4=20");

        assertThat(state.getParameters(), hasEntry("#4", BigDecimal.valueOf(20)));
    }

    @Test
    public void stateEquals()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N10 G00 G1 G2 G3");

        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "#3 = 10");

        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "F3000");

        GCodeState state4 = GCodeState.newInstance(Optional.of(state3), "N50 S20000");

        assertThat(state4.getModalWords(), containsInAnyOrder(Word.createPair('G', BigDecimal.valueOf(3)),
                Word.createPair('F', BigDecimal.valueOf(3000)),
                Word.createPair('S', BigDecimal.valueOf(20000))));

        assertThat(state4.getPreviousState().get(), equalTo(state3));
    }

    @Test
    public void getItererationForStates()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N10 G00 G1 G2 G3");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "#3=10");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "F3000");
        GCodeState state4 = GCodeState.newInstance(Optional.of(state3), "N50 S20000");

        assertThat(state1.getIteration(), equalTo(1));
        assertThat(state2.getIteration(), equalTo(2));
        assertThat(state3.getIteration(), equalTo(3));
        assertThat(state4.getIteration(), equalTo(4));

        assertThat(state4.getPreviousState().get().getIteration(), equalTo(3));
        assertThat(state4.getPreviousState().get().getPreviousState(), equalTo(Optional.<GCodeState>empty()));
    }

    @Test
    public void rawLinesEquals()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N10 G00 G1 G2 G3");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "#3 = 10");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "F3000");
        GCodeState state4 = GCodeState.newInstance(Optional.of(state3), "N50 S20000");

        assertThat(state4.getRawLine(), equalTo("N50 S20000"));
        assertThat(state4.getPreviousState().get().getRawLine(), equalTo("F3000"));
        assertThat(state4.getPreviousState().get().getPreviousState(), equalTo(Optional.<GCodeState>empty()));
    }

    @Test
    public void rawState1LinesEquals()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N10 G00 G1 G2 G3");

        assertThat(state1.getRawLine(), equalTo("N10 G00 G1 G2 G3"));
        assertThat(state1.getPreviousState(), Matchers.equalTo(Optional.<GCodeState>empty()));
    }

    @Test
    public void plainReaderSingleWord()
    {
        List<Word> words = GCodeState.wordsFromLine("F400");

        assertThat(words, contains(Word.createPair('F', BigDecimal.valueOf(400))));
    }

    @Test
    public void plainReaderSixWords()
    {
        List<Word> words = GCodeState.wordsFromLine("G21G40G49G61M6T1");
        List<Word> expectedWords = Arrays.asList(
                Word.createPair('G', BigDecimal.valueOf(21)),
                Word.createPair('G', BigDecimal.valueOf(40)),
                Word.createPair('G', BigDecimal.valueOf(49)),
                Word.createPair('G', BigDecimal.valueOf(61)),
                Word.createPair('M', BigDecimal.valueOf(6)),
                Word.createPair('T', BigDecimal.ONE));

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void lineFromeSpartanGCode()
    {
        List<Word> words = GCodeState.wordsFromLine("G1 X-47.8655 Y-27.9646 Z-0.147625");
        List<Word> expectedWords = Arrays.asList(
                Word.createPair('G', BigDecimal.valueOf(1)),
                Word.createPair('X', BigDecimal.valueOf(-47.8655)),
                Word.createPair('Y', BigDecimal.valueOf(-27.9646)),
                Word.createPair('Z', BigDecimal.valueOf(-0.147625)));

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void rejectComments()
    {
        List<Word> words = GCodeState.wordsFromLine("G1 X4.0994 Y0.6272 Z-0.120745 (random comment F100)");
        List<Word> expectedWords = Arrays.asList(
                Word.createPair('G', BigDecimal.valueOf(1)),
                Word.createPair('X', BigDecimal.valueOf(4.0994)),
                Word.createPair('Y', BigDecimal.valueOf(0.6272)),
                Word.createPair('Z', BigDecimal.valueOf(-0.120745)));

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void lineInsOeBigComment()
    {
        List<Word> words = GCodeState.wordsFromLine("(Match the \"point order\" with S20000 the XYZ position here)");
        List<Word> expectedWords = Collections.emptyList();

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void semiColonComment()
    {
        List<Word> words = GCodeState.wordsFromLine("G21G40G49G61M6T1 ;the rest of this line should be ignored G55");
        List<Word> expectedWords = Arrays.asList(
                Word.createPair('G', BigDecimal.valueOf(21)),
                Word.createPair('G', BigDecimal.valueOf(40)),
                Word.createPair('G', BigDecimal.valueOf(49)),
                Word.createPair('G', BigDecimal.valueOf(61)),
                Word.createPair('M', BigDecimal.valueOf(6)),
                Word.createPair('T', BigDecimal.ONE));

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void blankLine()
    {
        List<Word> words = GCodeState.wordsFromLine("");
        List<Word> expectedWords = Collections.emptyList();

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void lineIsSemiColonComment()
    {
        List<Word> words = GCodeState.wordsFromLine(";Match the \"point order\" with S20000 the XYZ position here");
        List<Word> expectedWords = Collections.emptyList();

        assertThat(words, equalTo(expectedWords));
    }

    @Test
    public void getStateAfter5Lines()
    {
        GCodeState state = GCodeState.newInstance(Optional.empty(), "G1 Z0");
        state = GCodeState.newInstance(Optional.of(state), "F200");
        state = GCodeState.newInstance(Optional.of(state), "G1 Z-0.147625");
        state = GCodeState.newInstance(Optional.of(state), "F1200");
        state = GCodeState.newInstance(Optional.of(state), "X-21.7785 Y-5.8365");

        List<Word> expectedList = Arrays.asList(Word.createPair('G', BigDecimal.ONE),
                Word.createPair('F', BigDecimal.valueOf(1200)),
                Word.createPair('Z', BigDecimal.valueOf(-0.147625)),
                Word.createPair('X', BigDecimal.valueOf(-21.7785)),
                Word.createPair('Y', BigDecimal.valueOf(-5.8365)));

        assertThat(state.getModalWords(), containsInAnyOrder(expectedList.get(0), expectedList.get(1),
                expectedList.get(2), expectedList.get(3), expectedList.get(4)));
    }

    @Test
    public void G2andG3Arcs_ShouldAlsoBeAddedAsModalWords()
    {
        GCodeState st = GCodeState.newInstance(Optional.empty(), "N0060 G02 X1.7849 Y0.9549 I-0.1669 J0.0953");
        st = GCodeState.newInstance(Optional.of(st), "N0070 X1.7359 Y0.9183 I-0.219 J0.2424");
        st = GCodeState.newInstance(Optional.of(st), "N0080 X1.7076 Y0.9026 I-0.2112 J0.3467");
        st = GCodeState.newInstance(Optional.of(st), "N0090 X1.6783 Y0.8889 I-0.2421 J0.4798");

        assertThat(st.getModalWords(), containsInAnyOrder(Word.createPair('G', BigDecimal.valueOf(2)),
                Word.createPair('X', BigDecimal.valueOf(1.6783)),
                Word.createPair('Y', BigDecimal.valueOf(0.8889))));
    }

    @Test
    public void getStateRawLineEuro()
    {
        GCodeState state = GCodeState.newInstance(Optional.empty(), "G1 Z0");
        state = GCodeState.newInstance(Optional.of(state), "F200");
        state = GCodeState.newInstance(Optional.of(state), "G1 Z-0,147625");
        state = GCodeState.newInstance(Optional.of(state), "F1200");
        state = GCodeState.newInstance(Optional.of(state), "X-21,7785 Y-5,8365");

        List<Word> expectedList = Arrays.asList(Word.createPair('G', BigDecimal.ONE),
                Word.createPair('F', BigDecimal.valueOf(1200)),
                Word.createPair('Z', BigDecimal.valueOf(-0.147625)),
                Word.createPair('X', BigDecimal.valueOf(-21.7785)),
                Word.createPair('Y', BigDecimal.valueOf(-5.8365)));

        assertThat(state.getModalWords(), containsInAnyOrder(expectedList.get(0), expectedList.get(1),
                expectedList.get(2), expectedList.get(3), expectedList.get(4)));
    }
}

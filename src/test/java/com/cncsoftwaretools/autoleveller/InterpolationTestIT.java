package com.cncsoftwaretools.autoleveller;

import com.cncsoftwaretools.autoleveller.util.AutolevellerUtil;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class InterpolationTestIT
{
    private RPF rpf = RPF.newInstance("src/test/ALProbeEMCSmall.test").get();

    @Test
    public void divideByZero()
    {
        Pair<BigDecimal, BigDecimal> pair1 = Pair.createPair(new BigDecimal("40"), new BigDecimal("5.628"));
        Pair<BigDecimal, BigDecimal> pair2 = Pair.createPair(new BigDecimal("40"), new BigDecimal("7.384"));

        Pair<BigDecimal, BigDecimal> answer = AutolevellerUtil.linearInterpolate(pair1, pair2).apply(new BigDecimal("38"));

        assertThat(answer.getSecond().setScale(3, BigDecimal.ROUND_HALF_UP), equalTo(new BigDecimal("5.628")));
    }

    @Test
    public void realInterpolation()
    {
        //from: http://www.eng.fsu.edu/~dommelen/courses/eml3100/aids/intpol/
        Pair<BigDecimal, BigDecimal> pair1 = Pair.createPair(new BigDecimal("35"), new BigDecimal("5.628"));
        Pair<BigDecimal, BigDecimal> pair2 = Pair.createPair(new BigDecimal("40"), new BigDecimal("7.384"));

        Pair<BigDecimal, BigDecimal> answer = AutolevellerUtil.linearInterpolate(pair1, pair2).apply(new BigDecimal(("38")));

        assertThat(answer.getSecond().setScale(3, BigDecimal.ROUND_HALF_UP), equalTo(new BigDecimal("6.682")));
    }

    @Test
    public void volumeOfSteam()
    {
        //from: http://www.eng.fsu.edu/~dommelen/courses/eml3100/aids/intpol/
        Pair<BigDecimal, BigDecimal> pair1 = Pair.createPair(new BigDecimal("10"), new BigDecimal("17.19561"));
        Pair<BigDecimal, BigDecimal> pair2 = Pair.createPair(new BigDecimal("50"), new BigDecimal("3.41833"));

        Pair<BigDecimal, BigDecimal> answer = AutolevellerUtil.linearInterpolate(pair1, pair2).apply(new BigDecimal("20"));

        assertThat(answer.getSecond().setScale(5, BigDecimal.ROUND_HALF_UP), equalTo(new BigDecimal("13.75129")));
    }

    @Test
    public void biLinearInterpolate_pointInSmallRPF()
    {
        Point2D pointToInterpolate = new Point2D(32.9, 49.07654);
        List<Point3D> surroundingPoints = rpf.getSurroundingPoints(pointToInterpolate);
        Point3D interpolatedPoint = AutolevellerUtil.biLinearInterpolate(surroundingPoints).apply(pointToInterpolate);

        assertThat(interpolatedPoint, equalTo(new Point3D(pointToInterpolate.getX(), pointToInterpolate.getY(), 0.10084)));
    }

    @Test
    public void biLinearInterpolate_topRightCorner()
    {
        Point2D pointToInterpolate = new Point2D(86.765, 76.67);
        List<Point3D> surroundingPoints = rpf.getSurroundingPoints(pointToInterpolate);
        Point3D interpolatedPoint = AutolevellerUtil.biLinearInterpolate(surroundingPoints).apply(pointToInterpolate);

        assertThat(interpolatedPoint, equalTo(new Point3D(pointToInterpolate.getX(), pointToInterpolate.getY(), 0.13154)));
    }

    @Test
    public void biLinearInterpolate_pointUncontained()
    {
        Point2D pointToInterpolate = new Point2D(123.456, 97.7);
        List<Point3D> surroundingPoints = rpf.getSurroundingPoints(pointToInterpolate);
        Point3D interpolatedPoint = AutolevellerUtil.biLinearInterpolate(surroundingPoints).apply(pointToInterpolate);

        assertThat(interpolatedPoint, equalTo(new Point3D(pointToInterpolate.getX(), pointToInterpolate.getY(), 0.11228)));
    }

    @Test
    public void biLinearInterpolate_pointOnLine()
    {
        Point2D pointToInterpolate = new Point2D(50, 34.4);
        List<Point3D> surroundingPoints = rpf.getSurroundingPoints(pointToInterpolate);
        Point3D interpolatedPoint = AutolevellerUtil.biLinearInterpolate(surroundingPoints).apply(pointToInterpolate);

        assertThat(interpolatedPoint, equalTo(new Point3D(pointToInterpolate.getX(), pointToInterpolate.getY(), 0.50285)));
    }
}

package com.cncsoftwaretools.autoleveller.util;

import com.cncsoftwaretools.autoleveller.Pair;
import com.cncsoftwaretools.autoleveller.reader.GCodeState;
import javafx.geometry.Point2D;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static com.cncsoftwaretools.autoleveller.matcher.Point2DMatcher.pointRoundedTo3DP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class ArcTest
{
    @Test
    public void angleForPoint()
    {
        assertThat(Math.round(Math.toDegrees(Arc.angleAtPoint(new Point2D(42.9, 32), new Point2D(17, 16.7)).doubleValue())), is(31L));
        assertThat(Math.round(Math.toDegrees(Arc.angleAtPoint(new Point2D(-7, 33), new Point2D(17, 16.7)).doubleValue())), is(146L));
        assertThat(Math.round(Math.toDegrees(Arc.angleAtPoint(new Point2D(5, -10), new Point2D(17, 16.7)).doubleValue())), is(246L));
        assertThat(Math.round(Math.toDegrees(Arc.angleAtPoint(new Point2D(30, -10), new Point2D(17, 16.7)).doubleValue())), is(296L));
        assertThat(Math.round(Math.toDegrees(Arc.angleAtPoint(new Point2D(18.5, 46), new Point2D(17, 16.7)).doubleValue())), is(87L));
        assertThat(Math.round(Math.toDegrees(Arc.angleAtPoint(new Point2D(44.5, 6), new Point2D(17, 16.7)).doubleValue())), is(339L));
    }

    @Test
    public void angleBetweenStartAndEnd_sameCWAsCCW()
    {
        Arc cwArc = Arc.newInstance(Point2D.ZERO, new Point2D(-4.098815, 8.893923), new Point2D(10, 10), Arc.Direction.CW);
        Arc ccwArc = Arc.newInstance(new Point2D(-4.098815, 8.893923), Point2D.ZERO, new Point2D(10, 10), Arc.Direction.CCW);

        double cwLength = Math.toDegrees(cwArc.angleBetweenStartAndGivenPoint(cwArc.getEndPoint()).doubleValue());
        double ccwLength = Math.toDegrees(ccwArc.angleBetweenStartAndGivenPoint(ccwArc.getEndPoint()).doubleValue());

        assertThat(ccwLength, equalTo(cwLength));
    }

    @Test
	public void angleBetweenStartAndEnd_clockwise()
	{
		Arc arc = Arc.newInstance(new Point2D(18.5, 46), new Point2D(44.5, 6), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc arc1 = Arc.newInstance(new Point2D(44.5, 6), new Point2D(18.5, 46), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc arc2 = Arc.newInstance(new Point2D(-7, 33), new Point2D(42.9, 32), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc arc3 = Arc.newInstance(new Point2D(30, -10), new Point2D(5, -10), new Point2D(17, 16.7), Arc.Direction.CW);

		assertThat(Math.round(Math.toDegrees(arc.angleBetweenStartAndGivenPoint(arc.getEndPoint()).doubleValue())), is(108L));
        assertThat(Math.round(Math.toDegrees(arc1.angleBetweenStartAndGivenPoint(arc1.getEndPoint()).doubleValue())), is(252L));
        assertThat(Math.round(Math.toDegrees(arc2.angleBetweenStartAndGivenPoint(arc2.getEndPoint()).doubleValue())), is(115L));
        assertThat(Math.round(Math.toDegrees(arc3.angleBetweenStartAndGivenPoint(arc3.getEndPoint()).doubleValue())), is(50L));
	}

    @Test
    public void angleBetweenStartAndEnd_counterClockwise()
    {
        Arc arc = Arc.newInstance(new Point2D(18.5, 46), new Point2D(44.5, 6), new Point2D(17, 16.7), Arc.Direction.CCW);
        Arc arc1 = Arc.newInstance(new Point2D(-7, 33), new Point2D(42.9, 32),  new Point2D(17, 16.7), Arc.Direction.CCW);
        Arc arc2 = Arc.newInstance(new Point2D(30, -10), new Point2D(5, -10), new Point2D(17, 16.7), Arc.Direction.CCW);

        assertThat(Math.round(Math.toDegrees(arc.
                angleBetweenStartAndGivenPoint(arc.getEndPoint()).doubleValue())), is(252L));
        assertThat(Math.round(Math.toDegrees(arc1.angleBetweenStartAndGivenPoint(arc1.getEndPoint()).doubleValue())), is(245L));
        assertThat(Math.round(Math.toDegrees(arc2.angleBetweenStartAndGivenPoint(arc2.getEndPoint()).doubleValue())), is(310L));
    }

    @Test
    public void angleBetweenStartAndEnd_fullCircles()
    {
        Arc arc360 = Arc.newInstance(new Point2D(42.9, 32), new Point2D(42.9, 32), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc arc360CCW = Arc.newInstance(new Point2D(42.9, 32), new Point2D(42.9, 32), new Point2D(17, 16.7), Arc.Direction.CCW);
        Arc arc180 = Arc.newInstance(new Point2D(42.9, 32), new Point2D(-8.9, 1.4), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc arc180CCW = Arc.newInstance(new Point2D(42.9, 32), new Point2D(-8.9, 1.4), new Point2D(17, 16.7), Arc.Direction.CCW);

        assertThat(Math.round(Math.toDegrees(arc360.angleBetweenStartAndGivenPoint(arc360.getEndPoint()).doubleValue())), is(0L));
        assertThat(Math.round(Math.toDegrees(arc360CCW.angleBetweenStartAndGivenPoint(arc360CCW.getEndPoint()).doubleValue())), is(0L));
        assertThat(Math.round(Math.toDegrees(arc180.angleBetweenStartAndGivenPoint(arc180.getEndPoint()).doubleValue())), is(180L));
        assertThat(Math.round(Math.toDegrees(arc180CCW.angleBetweenStartAndGivenPoint(arc180CCW.getEndPoint()).doubleValue())), is(180L));
    }

    @Test
    public void constructArcWhereOnlyRadiusIsKnown()
    {
        /*
         * From LinuxCNC doc: A positive radius indicates that the arc turns through less than 180 degrees,
         * while a negative radius indicates a turn of more than 180 degrees.
         * So, we know the startPoint, endPoint, direction (G2, G3) and the radius...
         * Pick the centre returned from Arc.getCentreFromRadius() which creates angle < 180 deg. if radius is positive
         * and angle > 180 deg. if radius is negative
         */
        Pair<Point2D, Point2D> centres = Arc.getCentreFromRadius(new Point2D(-64, 39), new Point2D(-50, 15), 20.0);
        Arc arcLT180 = Arc.newInstance(new Point2D(-64, 39), new Point2D(-50, 15), centres.getFirst(), Arc.Direction.CW);
        /* if radius was -20 Arc.getCentreFromRadius() would return the same centre points
         but now we choose the other centre point for our Arc*/
        Arc arcGT180 = Arc.newInstance(new Point2D(-64, 39), new Point2D(-50, 15), centres.getSecond(), Arc.Direction.CW);

        assertThat(arcLT180.getRadius(), equalTo(new BigDecimal(20)));
        assertThat(arcLT180.getCentrePoint(), pointRoundedTo3DP(new Point2D(-69.428, 19.751)));
        assertThat(arcGT180.getRadius(), equalTo(new BigDecimal(20))); // still radius of 20
        assertThat(arcGT180.getCentrePoint(), pointRoundedTo3DP(new Point2D(-44.572, 34.249)));

    }

    @Test
    public void getCentreFromRadius_oneEighty()
    {
        Pair<Point2D, Point2D> centres = Arc.getCentreFromRadius(new Point2D(-3, 0), new Point2D(3, 0), 3);

        // both centres should be the same
        assertThat(centres.getFirst(), pointRoundedTo3DP(Point2D.ZERO));
        assertThat(centres.getSecond(), pointRoundedTo3DP(Point2D.ZERO));
    }

    @Test
    public void getCentreFromRadius_29_5mmRadius_clockwise()
    {
        double radius = 29.5;

        assertThat(Arc.getCentreFromRadius(new Point2D(18.5, 46), new Point2D(44.5, 6),
                radius).getFirst(), pointRoundedTo3DP(new Point2D(16.948, 16.541)));
        assertThat(Arc.getCentreFromRadius(new Point2D(5.8, -9.9), new Point2D(-7, 34),
                radius).getFirst(), pointRoundedTo3DP(new Point2D(17.296, 17.268)));
        assertThat(Arc.getCentreFromRadius(new Point2D(30, -10), new Point2D(5.8, -9.9),
                radius).getFirst(), pointRoundedTo3DP(new Point2D(18.011, 16.954)));
        assertThat(Arc.getCentreFromRadius(new Point2D(-7, 34), new Point2D(5.8, -9.9),
                -radius).getSecond(), pointRoundedTo3DP(new Point2D(17.296, 17.268)));
    }

    @Test
    public void getCentreFromRadius_29_5mmRadius_CCW()
    {
        double radius = 29.5;

        assertThat(Arc.getCentreFromRadius(new Point2D(18.5, 46), new Point2D(-7, 33),
                radius).getSecond(), pointRoundedTo3DP(new Point2D(17.466, 16.518)));
        assertThat(Arc.getCentreFromRadius(new Point2D(30, -10), new Point2D(44.5, 6),
                radius).getSecond(), pointRoundedTo3DP(new Point2D(16.907, 16.435)));
        assertThat(Arc.getCentreFromRadius(new Point2D(44.5, 6), new Point2D(42.9, 32),
                radius).getSecond(), pointRoundedTo3DP(new Point2D(17.281, 17.374)));
        assertThat(Arc.getCentreFromRadius(new Point2D(18.5, 46), new Point2D(30, -10),
                -radius).getFirst(), pointRoundedTo3DP(new Point2D(17.106, 16.533)));
        assertThat(Arc.getCentreFromRadius(new Point2D(5, -10), new Point2D(-7, 33),
                -radius).getFirst(), pointRoundedTo3DP(new Point2D(17.577, 16.684)));
    }

    @Test
    public void getCentreFromRadius_14mmRadius_CCW()
    {
        double radius = 14;

        assertThat(Arc.getCentreFromRadius(new Point2D(-35, -46.1), new Point2D(-14, -60),
                -radius).getFirst(), pointRoundedTo3DP(new Point2D(-27.878, -58.153)));
    }

    @Test
    public void arcLength_Short()
    {
        Arc cwArc = Arc.newInstance(new Point2D(30, -10), new Point2D(5, -10), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc ccwArc = Arc.newInstance(new Point2D(30, -10), new Point2D(5, -10), new Point2D(17, 16.7), Arc.Direction.CCW);

        assertThat(Math.round(cwArc.getCircleCircumference().doubleValue()), equalTo(187L));
        assertThat(Math.round(cwArc.arcLength().doubleValue()), equalTo(26L));
        //ccwArc arc length should be 187 - 26 = 161
        assertThat(Math.round(ccwArc.arcLength().doubleValue()), equalTo(161L));
    }

    @Test
    public void angleForArcLength_segmentLength25()
    {
        BigDecimal cwAngle = Arc.angleForArcLength(new Point2D(15.91549, 0), Point2D.ZERO, 25, Arc.Direction.CW);
        BigDecimal ccwAngle = Arc.angleForArcLength(new Point2D(15.91549, 0), Point2D.ZERO, -25, Arc.Direction.CCW);

        assertThat(Math.round(Math.toDegrees(cwAngle.doubleValue())), is(270L));
        assertThat(Math.round(Math.toDegrees(ccwAngle.doubleValue())), is(90L));
    }

    @Test
    public void angleForArcLength_fullClockwiseCircle()
    {
        final Point2D centre = Point2D.ZERO;

        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(15.91549, 0), centre, 25, Arc.Direction.CW).doubleValue())), is(270L));

        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(0, -15.91549), centre, 25, Arc.Direction.CW).doubleValue())), is(180L));

        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(-15.91549, 0), centre, 25, Arc.Direction.CW).doubleValue())), is(90L));

        //rounded to 360 but is probably 359.9999999991 for example
        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(0, 15.91549), centre, 25, Arc.Direction.CW).doubleValue())), is(360L));
    }

    @Test
    public void angleForArcLength_fullCounterClockwiseCircle()
    {
        final Point2D centre = Point2D.ZERO;

        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(15.91549, 0), centre, 25, Arc.Direction.CCW).doubleValue())), is(90L));

        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(0, 15.91549), centre, 25, Arc.Direction.CCW).doubleValue())), is(180L));

        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(-15.91549, 0), centre, 25, Arc.Direction.CCW).doubleValue())), is(270L));

        //rounded to 360 but is probably 359.9999999991 for example
        assertThat(Math.round(Math.toDegrees(Arc.angleForArcLength(
                new Point2D(0, -15.91549), centre, 25, Arc.Direction.CCW).doubleValue())), is(360L));
    }

    @Test
    public void pointForArcLength_normalSegmentClockwiseAndCCwise()
    {
        Point2D startPoint = Point2D.ZERO;
        Point2D centrePoint = new Point2D(10,10);
        Point2D endPointCW = Arc.endPointForArcLength(startPoint, centrePoint, 10, Arc.Direction.CW);
        Arc newArc = Arc.newInstance(startPoint, endPointCW, centrePoint, Arc.Direction.CW);

        assertThat(Math.rint(newArc.arcLength().doubleValue()), equalTo(10.0));

        Point2D endPointCCW = Arc.endPointForArcLength(endPointCW, centrePoint, 10, Arc.Direction.CCW);

        Arc newArcReversed = Arc.newInstance(endPointCW, endPointCCW, centrePoint, Arc.Direction.CCW);

        assertThat(Math.rint(newArcReversed.arcLength().doubleValue()), equalTo(10.0));
        assertThat(endPointCCW, pointRoundedTo3DP(Point2D.ZERO));
    }

    @Test
    public void getPointOnCirumference_smallG3Arc()
    {
        Point2D startPoint = new Point2D(114.826, 165.195);
        Point2D centrePoint = new Point2D(110.422, 167.268);
        Point2D newEndPoint = Arc.endPointForArcLength(startPoint, centrePoint, 5, Arc.Direction.CCW);
        Arc newArc = Arc.newInstance(startPoint, newEndPoint, centrePoint, Arc.Direction.CCW);

        assertThat(newEndPoint, pointRoundedTo3DP(new Point2D(114.474, 169.965))); //new point shold be roughly here
        assertThat(Math.rint(newArc.arcLength().doubleValue()), is(5.0));
    }

    @Test
    public void verifyEquals()
    {
        //This test eliminates the warnings on Arc
        Arc arc1 = Arc.newInstance(new Point2D(30, -10), new Point2D(5, -10), new Point2D(17, 16.7), Arc.Direction.CW);
        Arc arc2 = Arc.newInstance(new Point2D(-4.098815, 8.893923), Point2D.ZERO, new Point2D(10, 10), Arc.Direction.CCW);
        Arc arc3 = Arc.newInstance(new Point2D(30, -10), new Point2D(5, -10), new Point2D(17, 16.7), Arc.Direction.CW);

        assertThat(arc1.getStartPoint(), pointRoundedTo3DP(new Point2D(30, -10)));
        assertThat(arc1.getEndPoint(), pointRoundedTo3DP(new Point2D(5, -10)));
        assertThat(arc1.getCentrePoint(), pointRoundedTo3DP(new Point2D(17, 16.7)));
        assertThat(arc2.getDirection(), equalTo(Arc.Direction.CCW));
        assertThat(arc1, equalTo(arc3));
        assertThat(arc1, not(equalTo(arc2)));
    }

    @Test
    public void getArcFromGCodeState_ShouldReturnValidArc()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N62730G00X262.277Y127.362");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "N62740G1Z-0.600F1200.0");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "N62750G2X260.712Y126.867I-1.692J2.630");

        Optional<Arc> arc = Arc.createArcFromState(state3);

        assertThat(arc.get().getRadius(), greaterThan(BigDecimal.ONE));
    }

    @Test
    public void getArcFromGCodeStateWithSpace_ShouldReturnValidArc()
    {
        GCodeState state1 = GCodeState.newInstance(Optional.empty(), "N50  X17.0518  Y18.9186");
        GCodeState state2 = GCodeState.newInstance(Optional.of(state1), "N55 G1 Z-127.0000 F60");
        GCodeState state3 = GCodeState.newInstance(Optional.of(state2), "N60 X17.0516 Y17.9186");
        GCodeState state4 = GCodeState.newInstance(Optional.of(state3), "N65 G2 X16.9784 Y17.6604 I16.5436 J17.9202");

        Optional<Arc> arc = Arc.createArcFromState(state4);

        assertThat(arc.get().getRadius(), greaterThan(BigDecimal.ONE));
    }
}
